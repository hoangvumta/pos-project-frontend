import { Routes, RouterModule }  from '@angular/router';
import { Layout } from './layout.component';
import { TokenGuard, AuthGuard } from '../helpers/guards';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  { path: '', component: Layout, children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
	{ path: 'dashboard', loadChildren: () => System.import('../dashboard/dashboard.module'), canActivate: [AuthGuard]},
    { path: 'another-page', loadChildren: () => System.import('../another/another.module'),  canActivate: [AuthGuard]},
    { path: 'san-pham', loadChildren: () => System.import('../products/products.module'), canActivate: [AuthGuard] },
    { path: 'khach-hang', loadChildren: () => System.import('../customers/customers.module') , canActivate: [AuthGuard]},

    { path: 'danh-muc', loadChildren: () => System.import('../category/category.module'), canActivate: [AuthGuard] },

    { path: 'nha-cung-cap', loadChildren: () => System.import('../suppliers/suppliers.module'), canActivate: [AuthGuard] },
    { path: 'tables', loadChildren: () => System.import('../tables/tables.module'), canActivate: [AuthGuard]},
    { path: 'nhan-vien', loadChildren: () => System.import('../employees/employees.module'), canActivate: [AuthGuard]},
    { path: 'kho-hang', loadChildren: () => System.import('../warehouse/warehouse.module'), canActivate: [AuthGuard]},
    { path: 'doanh-nghiep', loadChildren: () => System.import('../companies/companies.module'), canActivate: [AuthGuard] },
    { path: 'nguon-tien', loadChildren: () => System.import('../money-source/money-source.module') , canActivate: [AuthGuard]},
    { path: 'file-upload', loadChildren: () => System.import('../files-upload/file.module') , canActivate: [AuthGuard]},
    { path: 'du-bao-nhap-hang', loadChildren: () => System.import('../forecast/forecast.module') , canActivate: [AuthGuard]},
    { path: 'san-pham-moji', loadChildren: () => System.import('../moji_product/mjproduct.module') , canActivate: [AuthGuard]},
    { path: 'nhu-cau-nhap-hang', loadChildren: () => System.import('../chose_product/choseproduct.module') , canActivate: [AuthGuard]},
    { path: 'dang-xuat', loadChildren: () => System.import('../logout/logout.module') , canActivate: [AuthGuard]},
    { path: 'tai-khoan', loadChildren: () => System.import('../user/user.module') , canActivate: [AuthGuard]},
    { path: 'phieu-nhap-xuat', loadChildren: () => System.import('../coupon/coupon.module') , canActivate: [AuthGuard]},
    { path: 'phieu-chuyen-kho', loadChildren: () => System.import('../bill-warehouse-transport/bill-warehouse.module') , canActivate: [AuthGuard]},
    { path: 'canh-bao-nhap-hang', loadChildren: () => System.import('../cron/cron.module') , canActivate: [AuthGuard]},
  ]}
];


export const ROUTES = RouterModule.forChild(routes);
