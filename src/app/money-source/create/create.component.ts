import {Component, ViewEncapsulation, OnInit, Injectable,} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {MoneySourceService} from "../money-source.service";

@Component({
    selector: '[create]',
    templateUrl: './create.template.html',
    styleUrls: ['./create.style.scss'],
    providers: [MoneySourceService]
})
@Injectable()
export class Create implements OnInit{
    model = {
        name: '',
        notes: ''
    };
    constructor(private router: Router, private http: Http, private moneySourceService: MoneySourceService){}
    ngOnInit(): void {}

    addMoneySource(form: NgForm){
        this.moneySourceService.create(this.model).subscribe(
            (res: any) => {
                this.router.navigate(['/nguon-tien']);
            },
            error => {
                this.router.navigate(['/nguon-tien', 'them-moi']);
            }
        )
    }
}