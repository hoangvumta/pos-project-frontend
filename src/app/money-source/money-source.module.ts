import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {Create} from "./create/create.component";
import {ListMoneySource} from "./list-money-source/list-money-source.component";



import { AlertModule, TooltipModule } from 'ng2-bootstrap/ng2-bootstrap';
import { ButtonsModule, DropdownModule, PaginationModule  } from 'ng2-bootstrap/ng2-bootstrap';
import { DataTableDirectives } from 'angular2-datatable/datatable';
import { Ng2TableModule } from 'ng2-table';

// import { WidgetModule } from '../layout/widget/widget.module';
// import { UtilsModule } from '../layout/utils/utils.module';
import { JqSparklineModule } from '../components/sparkline/sparkline.module';
import {EditMoneySource} from "./edit-money-source/edit-money-source.component";

export const routes = [
    {path: '', redirectTo: 'create', pathMatch: 'full'},
    {path: 'them-moi', component: Create},
    {path: '', component: ListMoneySource},
    {path: ':id/cap-nhat', component: EditMoneySource},
];

@NgModule({
    declarations: [
        Create,
        ListMoneySource,
        DataTableDirectives,
        EditMoneySource
    ],
    imports: [
        CommonModule,
        FormsModule,


        JqSparklineModule,
        AlertModule,
        TooltipModule,
        ButtonsModule,
        DropdownModule,
        PaginationModule,
        // WidgetModule,
        // UtilsModule,
        Ng2TableModule,




        RouterModule.forChild(routes)
    ]
})
export default class StoreModule {
    static routes = routes;
}