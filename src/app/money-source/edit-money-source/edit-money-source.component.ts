import {Component, Injectable, OnInit} from "@angular/core";
import {MoneySourceService} from "../money-source.service";
import {Response} from "@angular/http";
import {Router, ActivatedRoute} from "@angular/router";
import {NgForm} from "@angular/forms";

@Component({
    selector: 'edit',
    templateUrl: './edit-money-source.template.html',
    styleUrls: ['./edit-money-source.style.scss'],
    providers: [MoneySourceService]
})

@Injectable()

export class EditMoneySource implements OnInit{
    constructor(private route: ActivatedRoute, private router: Router,  private moneySourceService: MoneySourceService){}
    model = [];
    id: any;
    ngOnInit(){
        this.route.params.subscribe(
            (params: any) => {
                this.id = params.id;
            }
        );
        this.moneySourceService.getDetail(this.id).subscribe(
            (res: any) => {
                this.model = res
            }
        )
    }

    editMoneySource(form: NgForm){
        this.moneySourceService.edit(this.model).subscribe(
            (res: any) => {
                this.router.navigate(['/nguon-tien']);
            },
            error => {
                this.router.navigate(['/nguon-tien', this.id, '/cap-nhat']);
            }
        )
    }
}