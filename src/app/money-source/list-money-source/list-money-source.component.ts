import {Component, Injectable, OnInit, Input} from "@angular/core";
import {MoneySourceService} from "../money-source.service";
import {Response} from "@angular/http";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";

@Component({
    selector: '[tables-basic]',
    templateUrl: './list-money-source.template.html',
    styleUrls: ['./list-money-source.style.scss'],
    providers: [MoneySourceService]
})

@Injectable()

export class ListMoneySource implements OnInit{

    index: number;
    constructor(private moneySourceService: MoneySourceService, private router: Router){}
    private datas = [];

    ngOnInit(){
        this.moneySourceService.getData().subscribe(
            response => {
                this.datas = response;
            },
            error => {
                console.log(error);
            }
        );
    }

    onCreate(){
        this.router.navigate(['/nguon-tien', 'them-moi']);
    }

    onEdit(id){
        this.router.navigate(['/nguon-tien', id, 'cap-nhat']);
    }

    onChange(form: NgForm){
        console.log(form.value);
    }
}