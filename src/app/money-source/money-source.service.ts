import { Injectable } from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {AppConfig} from '../app.config';
@Injectable()

export class MoneySourceService{
    constructor(private http: Http, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    public getData(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return this.http.get(this.config.baseApiUrl+'/currency/list-currency').map(res => res.json());
    }

    public create(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/currency/create',JSON.stringify(model),{
            headers: headers
        }).map((response: any) => response.json());
    }

    public edit(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/currency/update',JSON.stringify(model),{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public delete(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/currency/delete',{id: id},{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public getDetail(id){
        return this.http.get(this.config.baseApiUrl+'/currency/view?id='+id)
            .map((res) => res.json());
    }
}