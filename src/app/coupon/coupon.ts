export class Coupons {
    constructor(
        public id: number = null,
        public company_id: number = 0,
        public supplier_id: number = 0,
        public warehouse_id: number = 0,
        public user_id: number = 0,
        public notes: string = null,
        public vat: string = null,
        public vat_code: number = null,
        public vat_time: string = null,
        public total_cash: number = null,
        public total_bank_transfer: number = null,
        public discount: number = null,
        public final_price: number = 0,
        public discount_type: number = 1,
        public vat_type: number = 1,
        public type: number = 1

    ) {}
}