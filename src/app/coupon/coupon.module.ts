import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {Coupon} from "./create/create.component";
import {Bill} from "./list/list.component";
import {CouponService} from "./coupon.service";
import {WareHouseService} from "../warehouse/warehouse.service";
import {SimpleNotificationsModule} from "angular2-notifications/src/simple-notifications.module";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {ValidationService} from "../components/validation/validation.service";

import { PosSharedModule } from "../components/PosSharedModule";
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import 'ng2-datetime/src/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js';

export const routes = [
    {path: 'them-moi', component:Coupon},
    {path: ':id/cap-nhat', component:Coupon},
    {path: '', component:Bill}
];


@NgModule({
    declarations: [
        Coupon,
        Bill
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
        SimpleNotificationsModule,
		PosSharedModule,
        NKDatetimeModule

    ],
    providers: [CouponService,NotificationsService, WareHouseService, ValidationService]
})
export default class CouponModule {
    static routes = routes;
}
