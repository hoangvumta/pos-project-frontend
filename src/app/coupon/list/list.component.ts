import {Component, Injectable, OnInit, ViewEncapsulation} from "@angular/core";
import {NgForm} from "@angular/forms";
import {CouponService} from "../coupon.service";
import {UrlHelper} from "../../helpers/UrlHelper";
declare var $:any;

@Component({
  selector: '[tables-basic]',
  templateUrl: './list.template.html',
  styleUrls: ['./list.style.scss'],
  encapsulation: ViewEncapsulation.None
})

@Injectable()
export class Bill implements OnInit {

	constructor(
		private couponService: CouponService
	)
	{}


	time = {
		from: '',
		to: '',
		type: '',
		verify_status:''
	};
	

	ngOnInit():void {
		this.filterData();
	}

	showLoading = false;
	afterView = false;

	onFiltered(from: NgForm, to: NgForm, type:NgForm, verify_status:NgForm){
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;
		let filters = {'from': from.value, 'to': to.value, 'type':type.value, 'verify_status':verify_status.value};

		if(from.value && to.value){
			UrlHelper.insertParam('from', from.value);
			UrlHelper.insertParam('to', to.value);
		}
		if(type.value == ''){
			UrlHelper.insertParam('type', '');
		}else {
			UrlHelper.insertParam('type', type.value);
		}
		if(verify_status.value == ''){
			UrlHelper.insertParam('verify_status', '');
		}else {
			UrlHelper.insertParam('verify_status', verify_status.value);
		}
		this.couponService.getBill(page,itemPerpage,filters).subscribe(
			res => {
				this.data = res.data;
				this.time.from = res.filters['from'];
				this.time.to = res.filters['to'];
				// this.settings.pagination.totalItems = res.totalItems;
				// this.settings.pagination.currentPage = page;
				this.afterView = true;
			},
			error => {
				console.log('Error');
			},
			()=>this.showLoading = false
		);
	}

	//Config pos-table
	data = [
	];
	settings = {
		columns: [
			{
				label: '#',
				value: (model,index)=>{
					return index+1;
				}
			},
			{
				label: 'ID',
				value: (model)=>{
					let link = this.redirectLink(model.type);
					let html = '';
					html = '<a [routerLink]="[\'/'+link+model.id+'/cap-nhat\']">'+model.id+'</a>';
					return html;
				},
				format: 'raw'
			},
			{
				label: 'Loại phiếu',
				value: (model)=>{
					let link = this.redirectLink(model.type);
					let html = '';
					html = '<a [routerLink]="[\'/'+link+model.id+'/cap-nhat\']">'+model.name+'</a>';
					return html;
				},
				format: 'raw'
			},
			{
				label: 'Trạng thái',
				value: (model)=>{
					return (model.verify_status == 1? 'Đã duyệt': 'Chưa duyệt');
				}
			},
			{
				label: 'Thời gian',
				value: (model)=>{
					let html = '';
					html = model.created_time_hour + '<br/>' + model.created_time_day;
					return html;
				},
				format: 'raw'
			},
			{
				label: 'Thao tác',
				value: (model)=>{
					let link = this.redirectLink(model.type);
					let html = '';
					html = '<a [routerLink]="[\'/'+link+model.id+'/cap-nhat\']" class="glyphicon glyphicon-pencil edit"></a>';
					return html;
				},
				format: 'raw',
				style: {
					'text-align': 'right'
				}
			},
		],
		pagination: {
			currentPage: 1,
			maxSize: 5,
			itemsPerPage: 50,
			totalItems: 0,
		}
	};

	redirectLink(type){
		let link = '';
		switch (type){
			case '2':{
				link = 'phieu-chuyen-kho/';
				break;
			}
			default:
				link = 'phieu-nhap-xuat/';
				break;
		}
		return link;
	}

	pageChanged(event:any):void {
		this.filterData();
	};

	sortChanged(event){
		this.filterData();
	}

	filterChanged(event){
		this.filterData();
	}

	filterData(){
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;
		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();


		this.showLoading = true;
		this.couponService.getBill(page,itemPerpage,filters).subscribe(
			res => {
				this.data = res.data;
				this.time.from = res.filters['from'];
				this.time.to = res.filters['to'];
				this.time.type = res.filters['type'];
				this.time.verify_status = res.filters['verify_status'];
				this.settings.pagination.totalItems = res.totalItems;
				this.settings.pagination.currentPage = page;
				this.afterView = true;
			},
			error => {
				console.log('Error');
			},
			()=>this.showLoading = false
		)
	}

	//End config pos-table


	ngOnDestroy() {}

	ngAfterViewChecked(){
		if(this.afterView){
			if($('.delete-store').length > 0){
				$('.delete-store').off('click').click((ele)=>{
					let id = $(ele.currentTarget).attr('data-id');
					this.onDelete(id);
				});
				this.afterView = false;
			}else{
			}
		}
	}


	onDelete(id) {
		this.showLoading = true;
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;

		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();
		this.couponService.deleteBill(id).subscribe(
			(res:any) => {
				if(res.success == 1){
					this.filterData();
				}
			},
			error => {
				console.log('Error')
			},
			()=> this.showLoading = false
		)
	}

}