import {Component, Injectable, OnInit, Renderer, ElementRef, ViewChild,ViewEncapsulation} from "@angular/core";
import {UrlHelper} from "../../helpers/UrlHelper";
import { Coupons} from "../coupon.ts";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {WareHouseService} from "../../warehouse/warehouse.service";
import {CouponService} from "../coupon.service";
import {Subscription} from "rxjs/Rx";
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { Router, ActivatedRoute} from "@angular/router";
declare var $:any;
@Component({
    selector: '[tables-basic]',
    host: {
        '(document:click)': 'handleClick($event)',
    },
    templateUrl: './create.template.html',
    styleUrls: ['./create.style.scss'],
    encapsulation: ViewEncapsulation.None
})

@Injectable()
export class Coupon implements OnInit {
    @ViewChild('selector') selector: ElementRef;
    model = new Coupons();
    listenFunc:Function;
    showLoading = false;
    afterView = false;
    exampleSupplier = [];
    exampleCompany = [];
    exampleWarehouse = [];
    exampleUser = [];

    list_product_id = [];
    idChecked = [];
    changeComponent = false;

    differ:any;
    couponId = null;
    verify_status = null;
    private subscription: Subscription;
    couponForm: FormGroup;
    
    discount_type: string = '%';
    vat_type: string = '%';

    //Search Product
    public query = '';
    public filteredList = [];
    public message = '';
    public warning = '';

    total_cost = 0 ;

    constructor(
        private elementRef:ElementRef,
        private renderer:Renderer,
        private noti: NotificationsService,
        private warehouseService: WareHouseService,
        private couponService: CouponService,
        private location:Location,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.elementRef = elementRef;
    }
    

    //Notification
    optionsNoti = {
        position: ["bottom", "right"],
        timeOut: 1000,
    };

    ngOnInit():void {
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if(params.hasOwnProperty('id')) {
                    this.couponId = params.id;
                }else {
                    this.couponId = null;
                }
                this.initForm();
            }
        );
        this.warehouseService.getCompany().subscribe(
            data => {
                if(data.length == 1){
                    this.model.company_id = data[0].id;
                    this.couponService.getWarehouse(this.model.company_id).subscribe(
                        res => {
                            this.exampleWarehouse = res
                        }
                    );
                }else {
                    data.unshift({id: 0, text: 'Doanh nghiệp'});
                }
                this.exampleCompany = data;
            }
        );
        this.couponService.getSupplier().subscribe(
            res => {
                res.unshift({id: 0, text: 'Nhà cung cấp'});
                this.exampleSupplier = res;
            }
        );
        this.couponService.getUser().subscribe(
            res => {
                res.unshift({id: 0, text: 'Nhân viên'});
                this.exampleUser = res;
            }
        );
    }

    validateNumber(){
        $('#vat , #total_bank_transfer , #total_cash, #discount, #datepicker1').keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

    change_discount_type(type) {
        this.discount_type = type;
        if(type == '%'){
            this.model.discount_type = 1;
        }else if(type == 'vnđ'){
            this.model.discount_type = 2;
        }
    }

    change_vat_type(type) {
        this.vat_type = type;
        if(type == '%'){
            this.model.vat_type = 1;
        }else if(type == 'vnđ'){
            this.model.vat_type = 2;
        }
    }
    
    filter() {
        this.message = '';
        if (this.query !== "") {
            this.couponService.getInfoProduct(this.query).subscribe(
                response => {
                    this.filteredList = response;
                },
                error => {
                    console.log(error);
                }
            );
        } else {
            this.filteredList = [];
        }
    }

    select(item) {
        this.query = "";
        if (this.list_product_id.length != 0) {
            let flag = 1;
            for (let i = 0; i < this.list_product_id.length; i++) {
                if (item.id_nhanh == this.list_product_id[i]['id_nhanh']) {
                    flag = 0;
                    // this.message = ' Sản phẩm đã có trong bảng'
                }
            }
            if (flag == 1) {
                this.list_product_id.push(item);
            }
        } else {
            this.list_product_id.push(item);
        }
        this.filteredList = [];
    }

    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
    }

    initForm() {
        //Form Products
        if(this.couponId != null){
            this.couponService.getCouponDetail(this.couponId).subscribe(
                (data:any)=>{
                    if(data.info == null){
                        this.router.navigate(['/error']);
                    }
                    else {
                        this.couponForm = this.formBuilder.group({
                            company_id: ['', Validators.required],
                            warehouse_id: ['', Validators.required],
                            supplier_id: ['', Validators.required],
                            notes: new FormControl(),
                            vat: new FormControl(),
                            vat_code: ['', Validators.required],
                            vat_time: new FormControl(),
                            total_cash: new FormControl(),
                            total_bank_transfer: new FormControl(),
                            discount: new FormControl(),
                            final_price: new FormControl(),
                            search : new FormControl()
                        });
                        this.model = data.info;
                        this.model.warehouse_id = data.info.to;
                        this.model.supplier_id = data.info.from;
                        this.verify_status = data.info.verify_status;
                        this.couponService.getWarehouse(this.model.company_id).subscribe(
                            res => {
                                this.exampleWarehouse = res
                            }
                        );

                        this.list_product_id = data.products;
                        this.list_product_id.forEach((item, index)=> {
                            this.idChecked.push(item.id);
                        });
                        // this.dataQuantity = data.products;

                        this.filterData();
                        this.changeComponent = true;
                    }

                },
                (error)=>console.log(error)
            )

        }
        else{
            this.couponForm = this.formBuilder.group({
                company_id: [this.model.company_id, Validators.required],
                warehouse_id: [this.model.warehouse_id, Validators.required],
                supplier_id: [this.model.supplier_id, Validators.required],
                notes: new FormControl(),
                vat: new FormControl(),
                vat_code: ['', Validators.required],
                vat_time: new FormControl(),
                total_cash: new FormControl(),
                total_bank_transfer: new FormControl(),
                discount: new FormControl(),
                final_price: new FormControl(),
                search : new FormControl()
            });
        }

    }

    onSubmit(couponForm){
        if(this.couponId){
            this.showLoading = true;
            this.model.vat_time = $($('#datepicker1').find('input')[0]).val();
            if(this.model.company_id == 0 || this.model.warehouse_id == 0 || this.model.supplier_id == 0 || this.model.user_id == 0 || this.list_product_id.length == 0 ){
                this.message = 'Vui lòng điền đủ thông tin!';
                this.warning = 'Chưa có thông tin sản phẩm !Vui lòng chọn sản phẩm!';
                return false ;
            }
            this.couponService.edit(this.model,this.couponId, this.list_product_id).subscribe(
                (data:any) =>{
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Lưu thành công'
                        );
                    }else{
                        for (let propertyName in data.errors) {
                            this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.showLoading = false
            )
        }else{
            this.showLoading = true;
            this.model.vat_time = $($('#datepicker1').find('input')[0]).val();
            if(this.model.company_id == 0 || this.model.warehouse_id == 0 || this.model.supplier_id == 0 || this.model.user_id == 0 || this.list_product_id.length == 0 ){
                this.message = 'Vui lòng điền đủ thông tin!';
                this.warning = 'Chưa có thông tin sản phẩm !Vui lòng chọn sản phẩm!';
                return false ;
            }

            this.couponService.create(this.model, this.list_product_id).subscribe(
                (data:any) =>{
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Đã thêm phiếu mới'
                        );
                        this.couponId = data.id;
                        this.location.replaceState("/phieu-nhap-xuat/"+this.couponId+"/cap-nhat");
                    }else{
                        for (let propertyName in data.errors) {
                            this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.showLoading = false
            )
        }
    }


    //Config pos-table
    data = [];
    settings = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: '',
                value: (model, index)=> {
                    let id = model.id;
                    let checked = '';

                    if (this.idChecked.indexOf(id) > -1) {
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    let html = '<label>' +
                        '<input class="select-product" sp_id="' + id + '" data_id="' + index + '" type="checkbox" ' + checked + '>' +
                        '</label>';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                },
                sort: {attribute: 'id_nhanh'},
                filter: {
                    attribute: 'id_nhanh',
                    type: 'input'
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                },
                sort: {attribute: 'sku'},
                filter: {
                    attribute: 'sku',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            }
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    };

    settings2 = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: 'ID',
                value: (model)=> {
                    return model.id;
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                }
            },
            {
                label: 'Giá',
                value: (model)=> {
                    let val = '';
                    if(model.price){
                        val = this.number_format(model.price,0, ',', '.' );
                    }
                    let html = '<input type="text" class="form-control select-price" data-id="' + model.id + '"  value="'+val+'" />';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'Số lượng',
                value: (model)=> {
                    let val = '';
                    if(model.quantity){
                        val = this.number_format(model.quantity,0, ',', '.' );
                    }
                    let html = '<input type="text" class="form-control select-quantity" data-id="' + model.id + '" value="'+val+'" />';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'Chiết khấu',
                value: (model)=> {
                    let val = '';
                    if(model.discount){
                        val = model.discount;
                    }

                    let html = '<div class="input-group">' +
                                    '<input class="form-control select-discount" data-id="' + model.id + '" value="'+val+'">' +
                                    '<div class="input-group-btn"> ' +
                                        '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+(model.discount_type == 1?'%':'vnđ')+'<span class="caret"></span></button>' +
                                        ' <div class="dropdown-menu">' +
                                            ' <a class="dropdown-item person" data-id="' + model.id + '" href="javascript:;" >%</a>' +
                                            ' <a class="dropdown-item person" data-id="' + model.id + '" href="javascript:;" >vnđ</a>' +
                                        ' </div> ' +
                                    '</div> ' +
                                '</div>';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'Thành tiền',
                value: (model)=> {
                    let total;
                    let tmp = 0;
                    if(model.price && model.quantity && model.discount && model.discount_type){
                        if(model.discount_type == 1){
                            if(model.discount < 100){
                                tmp = ((parseFloat(model.price) * parseInt(model.quantity) * (100 - (parseFloat(model.discount)))))/100;
                            }else {
                                this.noti.error(
                                    'Lỗi!',
                                    'Chiết khấu phải nhỏ hơn 100'
                                );
                            }
                        }else {
                            tmp = (parseFloat(model.price)* parseInt(model.quantity)) - parseFloat(model.discount);
                        }

                        if(this.couponId){
                            this.total_cost += tmp/2;
                        }else {
                            this.total_cost += tmp;
                        }
                        this.model.final_price = this.total_cost;

                        total = this.number_format(tmp, 0, ',', '.');
                    }
                    let html = '<input type="text" class="form-control total_money"  readonly value="'+(total?total:0)+'"/>';
                    return html;
                },
                format: 'raw'
            },

            {
                label: '',
                value: (model)=> {
                    let html = '<a class="glyphicon glyphicon-trash remove-choice" style="margin-top: 10px" data-id="' + model.id_nhanh + '"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    };

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event) {
        this.filterData();
    }

    filterChanged(event) {
        this.filterData();
    }

    onVerify(id){
        this.couponService.verifyBill(id).subscribe(
            (data:any) =>{
                if(data.success == 1){
                    this.noti.success(
                        'Thành Công!',
                        'Đã duyệt phiếu'
                    );
                    this.router.navigate(['/phieu-nhap-xuat']);
                }else{
                    for (let propertyName in data.errors) {
                        this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                    }
                }
            },
            err => console.log(err),
            () => this.showLoading = false
        )
    }

    filterData() {
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.couponService.getProduct(page, itemPerpage, filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        );
        let that = this;
        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {

            $('.remove-choice').off('click').click((e)=> {
                let id = $(e.target).attr('data-id');
                this.list_product_id.forEach((item, i)=> {
                    if (item.id_nhanh == id) {
                        this.list_product_id.splice(i, 1);
                    }
                })
                this.idChecked.forEach((sid, i)=> {
                    if (sid == id) {
                        this.idChecked.splice(i, 1);
                    }
                });
            });

            $('.person').off('click').click((e) => {
                let id = $(e.target).attr('data-id');
                let discount_type = 1;
                if(e.target.text == 'vnđ'){
                    discount_type = 2;
                }
                that.total(id, 'discount_type', discount_type);
            });


            // textbox is number
            $('.select-price , .select-quantity ,.select-discount ').keydown(function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $('.select-price').off('change').change((e)=> {
                let id = $(e.target).attr('data-id');
                let price = $(e.target).val();
                that.total(id, 'price', price);
                // console.log(that.list_product_id)
            });

            $('.select-quantity').off('change').change((e)=> {
                let id = $(e.target).attr('data-id');
                let quantity = $(e.target).val();
                that.total(id, 'quantity', quantity);
            });

            $('.select-discount').off('change').change((e)=> {
                let id = $(e.target).attr('data-id');
                let discount = $(e.target).val();
                that.total(id, 'discount', discount);
            });
        });
    }

    number_format(number, decimals, dec_point, thousands_sep) {

        number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
        let n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = [],
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
         s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }

    total(id, attr, param){
        // let total_money = 0;
        let flag = 1;
        if(this.list_product_id.length > 0){
            this.list_product_id.forEach((item, index) => {
                if(item.id == id){
                    switch (attr){
                        case 'price':{
                            this.list_product_id[index].price = param;
                            break;
                        }
                        case 'quantity':{
                            this.list_product_id[index].quantity = param;
                            break;
                        }
                        case 'discount':{
                            this.list_product_id[index].discount = param;
                            break;
                        }
                        case 'discount_type':{
                            this.list_product_id[index].discount_type = param;
                            break;
                        }
                    }
                    if(this.list_product_id[index].quantity && this.list_product_id[index].price && this.list_product_id[index].discount || this.discount_type){
                        this.changeComponent = (this.changeComponent)?false:true;
                        this.total_cost = 0 ;
                    }
                    flag = 0;
                }
            });
        }
    }


    //End config pos-table
    onClickOpenModal() {
        this.filterData();
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private afterSelectFilterRender = true;
    ngAfterViewChecked() {
        if($('.filter-select option').length > 0 && this.afterSelectFilterRender){
            $('.filter-select').select2({
                theme: 'bootstrap',
                width: '100%'
            });
            $('.filter-select').change((event)=>{
                if($(event.target).attr('name') == 'company_id'){
                    this.model.company_id = event.target.value;
                    this.couponService.getWarehouse(event.target.value).subscribe(
                        res => {
                            res.unshift({id: 0, text: 'Kho hàng'});
                            this.exampleWarehouse = res
                        }
                    );
                }
                if($(event.target).attr('name') == 'warehouse_id'){
                    this.model.warehouse_id = event.target.value;
                }
                if($(event.target).attr('name') == 'supplier_id'){
                    this.model.supplier_id = event.target.value;
                }
                if($(event.target).attr('name') == 'user_id'){
                    this.model.user_id = event.target.value;
                }
                let attr = event.target.attributes.name.value;
                let value = event.target.value;
            });
            this.afterSelectFilterRender =false;
        }

        if (this.afterView) {
            if ($('.select-product').length > 0) {
                let that = this;
                $('.select-product').off('click').on('click', function (e) {

                    let id = $(this).attr('data_id');
                    let spid = $(this).attr('sp_id');

                    if ($(this).prop('checked')) {
                        $(this).closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                        that.data.forEach((item, index)=> {
                            if (item.id == spid) {
                                item['discount_type'] = 1;
                                that.list_product_id.push(item);
                                that.total_cost = 0 ;
                            }
                        })
                        that.idChecked.push(spid);
                    } else {
                        that.total_cost = 0;
                        $(this).closest('tr').css("background-color", "white");
                        that.list_product_id.forEach((item, i)=> {
                            if (item.id == spid) {
                                that.list_product_id.splice(i, 1);
                            }
                        })
                        that.idChecked.forEach((sid, i)=> {
                            if (sid == spid) {
                                that.idChecked.splice(i, 1);
                            }
                        })
                    }
                });
                $('.select-product:checked').closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                this.afterView = false;
            }
        }

    }

}