import {Component, OnInit} from '@angular/core';
import {Http} from "@angular/http";

@Component({
  selector: '[tables-basic]',
  template: require('./tables-basic.template.html')
})
export class TablesBasic implements OnInit{
  
  datas = [];
  constructor(private http: Http){
    
  }
  
  ngOnInit() {
    this.http.post('http://localhost/adobe-app/db.php', '').subscribe(
        (data) =>  this.datas = data.json()
    );
  }
  

}
