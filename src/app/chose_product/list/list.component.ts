import {Component, Injectable, OnInit, Renderer, ElementRef, ViewChild, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {UrlHelper} from "../../helpers/UrlHelper";
import {ChoseProductService} from "../../chose_product/choseproduct.service";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
declare var $:any;
@Component({
    selector: '[tables-basic]',
    templateUrl: './list.template.html',
    styleUrls: ['./list.style.scss']
})

@Injectable()
export class ChoseProduct implements OnInit {
    @ViewChild('selector') selector: ElementRef;
    listenFunc:Function;
    globalListenFunc:Function;

    differ:any;

    constructor(
        private choseproductService:ChoseProductService,
        private elementRef:ElementRef,
        private renderer:Renderer,
        private noti: NotificationsService
    ) {

    }

    type = 1; //san pham dang bán = 1; sản phẩm mới =2
    new_type = 0; //1 2 3
    item_type = 0;
    dataOption = [];
	htmlResult = '';
	ShowTSforNewCategory = false;
	TSforNewCategory:number = 0;

    onChangeNewType(e){
        this.new_type = e.target.value;
        this.dataOption = [];
        if(this.new_type == 1){
            this.choseproductService.getCategory().subscribe(
                (data)=>{
                    this.dataOption = data;
                },
                (err)=>console.log(err)
            )
        }else if(this.new_type == 2){
            this.choseproductService.getTags().subscribe(
                (data)=>{
                    this.dataOption = data;
                },
                (err)=>console.log(err)
            )
        }

        $(this.selector.nativeElement).select2({
            theme: 'default',
            width: '100%',
			
        });
    }
	
	onChangeTags(e){
        let tags = $(this.selector.nativeElement).select2('val');
		console.log('on change tags');
		if(tags.length == 0) {
			console.log('day');
			this.ShowTSforNewCategory = true;
		} else {
			this.ShowTSforNewCategory = false;
			console.log('day ne');
		}
    }

    //Notification
    optionsNoti = {
        position: ["top", "right"],
        timeOut: 3000
    }

    ngOnInit():void {
        //this.filterData();
    }

    totalDayGetData:number = 42;
    totalDaySoldOut:number = 30;

    showLoading = false;
    afterView = false;

    list_product_id = [];
    idChecked = [];

    //Config pos-table
    data = [];
    settings = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: '',
                value: (model, index)=> {
                    let id = model.id_nhanh;
                    let checked = '';

                    if (this.idChecked.indexOf(id) > -1) {
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    let html = '<label>' +
                        '<input class="select-product" sp_id="' + id + '" data_id="' + index + '" type="checkbox" ' + checked + '>' +
                        '</label>';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                },
                sort: {attribute: 'id_nhanh'},
                filter: {
                    attribute: 'id_nhanh',
                    type: 'input'
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                },
                sort: {attribute: 'sku'},
                filter: {
                    attribute: 'sku',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            }
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    settings2 = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                }
            },
            {
                label: 'Kết quả',
                value: (model)=> {
                    return '';
                }
            },
            {
                label: '',
                value: (model)=> {
                    let html = '<a class="glyphicon glyphicon-trash remove-choice" data-id="' + model.id_nhanh + '"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event) {
        this.filterData();
    }

    filterChanged(event) {
        this.filterData();
    }

    filterData() {
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.choseproductService.getProduct(page, itemPerpage, filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        )

        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {
            $('.remove-choice').off('click').click((e)=> {
                let id = $(e.target).attr('data-id');
                this.list_product_id.forEach((item, i)=> {
                    if (item.id_nhanh == id) {
                        this.list_product_id.splice(i, 1);
                    }
                })
                this.idChecked.forEach((sid, i)=> {
                    if (sid == id) {
                        this.idChecked.splice(i, 1);
                    }
                })
            })
        });
    }


    //End config pos-table
    onClickOpenModal() {
        this.filterData();
    }


    ngOnDestroy() {
        //this.listenFunc();
        //this.globalListenFunc();
    }

    ngAfterViewChecked() {
        if (this.afterView) {
            if ($('.select-product').length > 0) {
                console.log('vao day');
                let that = this;
                $('.select-product').off('click').on('click', function (e) {

                    let id = $(this).attr('data_id');
                    let spid = $(this).attr('sp_id');

                    if ($(this).prop('checked')) {
                        $(this).closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                        that.data.forEach((item)=> {
                            if (item.id_nhanh == spid) {
                                that.list_product_id.push(item);
                            }
                        })
                        that.idChecked.push(spid);
                    } else {
                        $(this).closest('tr').css("background-color", "white");
                        that.list_product_id.forEach((item, i)=> {
                            if (item.id_nhanh == spid) {
                                that.list_product_id.splice(i, 1);
                            }
                        })
                        that.idChecked.forEach((sid, i)=> {
                            if (sid == spid) {
                                that.idChecked.splice(i, 1);
                            }
                        })
                    }
                });
                $('.select-product:checked').closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                this.afterView = false;
            }
        }
    }
    changeComponent = false;

    calculateImporting() {
		this.htmlResult = '';
        $('.simple-notification-wrapper').css("z-index",9999999999);
        let flag = true;

        if(this.type == 1){
            if(this.idChecked.length > 0){

            }else{
                flag = false;
                this.noti.error(
                    'Lỗi!',
                    'Phải chọn sản phẩm trước khi tính'
                );
                return;
            }
        } else if(this.type ==2) {
			if(this.new_type == 0) {
				flag = false;
					this.noti.error(
						'Lỗi!',
						'Chọn cách tính'
					);
					return;
			} else if(this.new_type == 2) {
				let tagIds = $(this.selector.nativeElement).select2('val');
				if(tagIds.length == 0) {
					flag = false;
					this.noti.error(
						'Lỗi!',
						'Chọn tag sản phẩm'
					);
					return;
				}
			} else if(this.new_type == 3) {
				if(this.idChecked.length == 0) {
					flag = false;
					this.noti.error(
						'Lỗi!',
						'Phải chọn sản phẩm'
					);
					return;
				}
			}
		}

        if(this.totalDayGetData > 0){

        }else{
            flag = false;
            this.noti.error(
                'Lỗi!',
                'Phải nhập số ngày lấy dữ liệu'
            );
            return;
        }

        if(this.totalDaySoldOut > 0){

        }else{
            flag = false;
            this.noti.error(
                'Lỗi!',
                'Phải nhập số ngày để bán hết'
            );
            return;
        }

        if(flag){
            this.showLoading = true;
			let tagIds = [];
			if(this.type == 2) {
				tagIds = $(this.selector.nativeElement).select2('val');
			}
			
            this.choseproductService.calculateImporting(this.type,this.totalDayGetData,this.totalDaySoldOut,this.idChecked,this.new_type, tagIds).subscribe(
                (data)=> {
                    this.changeComponent = (this.changeComponent)?false:true;
					
					if(data.type == 1) {
						let result = data.data;
						this.settings2.columns[4] = {
							label: 'Kết quả',
							value: (model)=> {
								let html = ''
								if(result[model.id_nhanh]) {
									html = 't = '+result[model.id_nhanh].timeToTakeData+'<br>';
									html += 'SL = '+result[model.id_nhanh].totalSoldOut+'<br>';
									html += 'TK = '+result[model.id_nhanh].inventory+'<br>';
									html += 'N = '+result[model.id_nhanh].needImport
								} else {
									html = '';
								}
	;
								return html;
							},
							format: 'raw'
						}
					} else if (data.type == 2) {
						switch(data.newType) { 
							case 1: {
								this.htmlResult = 'Đã bán của category: ' + data.data.totalSoldOutOfCategory + '<br>';
								this.htmlResult += 'Đã bán toàn hệ thống: ' + data.data.totalSoldOut + '<br>';
								this.htmlResult += 'Tỷ suất: ' + data.data.TS + '<br>';
								this.htmlResult += 'Cần nhập: ' + data.data.needImport + '<br>';
								break; 
							} 
							case 2: {
								this.htmlResult = 'Đã bán của sản phẩm theo tag: ' + data.data.totalSoldOut + '<br>';
								this.htmlResult += 'Cần nhập: ' + data.data.needImport + '<br>';
								break; 
							}
							case 3: {
								this.htmlResult = 'Đã bán của các sản phẩm đã chọn: ' + data.data.totalSoldOut + '<br>';
								this.htmlResult += 'Cần nhập: ' + data.data.needImport + '<br>';
								break; 
							}
						} 
					}
					
                    this.showLoading = false;
                },
                (err)=>console.log(err)
            )
        }
    }

    changedTable(event){

    }
}