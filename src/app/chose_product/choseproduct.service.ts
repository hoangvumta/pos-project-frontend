import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import { ApiService } from '../helpers/api.service';
import {AppConfig} from '../app.config';
@Injectable()

export class ChoseProductService{
    constructor(private http: Http, private localStorageService: LocalStorage, private apiService: ApiService, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;


    getProduct(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/product/get-product-moji?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }

    calculateImporting(type, totalDay, totalSold, ids, newType, itemType){
		
		let body = {
				'type': type,
				'totalTimeToTakeData': totalDay,
				'totalTimeToSoldOut': totalSold,
				'productIds': ids,
				'newType': newType,
				'tagIds': itemType
			};

        return this.apiService.postJson(this.config.baseApiUrl+'/moji/calculate-importing', body).map(res => res)
    }

    addQuantity(data){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/moji/add-quantity','data='+JSON.stringify(data),{headers: headers}).map(res => res.json())
    }

    getCategory(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        return this.http.get(this.config.baseApiUrl+'/moji/list-category', {headers:headers})
            .map(res => res.json())
    }

    getTags(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        return this.http.get(this.config.baseApiUrl+'/moji/list-tag', {headers:headers})
            .map(res => res.json())
    }
    
}