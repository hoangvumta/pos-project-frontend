import {Component, Injectable, OnInit, Renderer, ElementRef, ViewChild, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {UrlHelper} from "../../helpers/UrlHelper";
import {ChoseProductService} from "../../chose_product/choseproduct.service";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
declare var $:any;
@Component({
    selector: '[tables-basic]',
    templateUrl: './list.template.html',
    styleUrls: ['./list.style.scss']
})

@Injectable()
export class ChoseProduct2 implements OnInit {
    @ViewChild('selector') selector: ElementRef;
    listenFunc:Function;
    globalListenFunc:Function;

    differ:any;

    constructor(
        private choseproductService:ChoseProductService,
        private elementRef:ElementRef,
        private renderer:Renderer,
        private noti: NotificationsService
    ) {

    }

    //Notification
    optionsNoti = {
        position: ["top", "right"],
        timeOut: 3000
    }

    ngOnInit():void {
        //this.filterData();
    }

    totalDayGetData:number;
    totalDaySoldOut:number;

    showLoading = false;
    afterView = false;

    list_product_id = [];
    idChecked = [];

    //Config pos-table
    data = [];
    settings = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: '',
                value: (model, index)=> {
                    let id = model.id_nhanh;
                    let checked = '';

                    if (this.idChecked.indexOf(id) > -1) {
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    let html = '<label>' +
                        '<input class="select-product" sp_id="' + id + '" data_id="' + index + '" type="checkbox" ' + checked + '>' +
                        '</label>';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                },
                sort: {attribute: 'id_nhanh'},
                filter: {
                    attribute: 'id_nhanh',
                    type: 'input'
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                },
                sort: {attribute: 'sku'},
                filter: {
                    attribute: 'sku',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            }
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    settings2 = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                }
            },
            {
                label: 'Số lượng nhập',
                value: (model)=> {
                    let val = (this.dataQuantity[model.id_nhanh])?this.dataQuantity[model.id_nhanh]:'';
                    let html = '<input type="text" class="form-control input-nhanh" id-nhanh="'+model.id_nhanh+'" value="'+val+'">';
                    return html;
                },
                format:'raw'
            },
            {
                label: 'Phân bổ',
                value: (model)=> {
                    return '';
                },
                format:'raw'
            },
            {
                label: '',
                value: (model)=> {
                    let html = '<a class="glyphicon glyphicon-trash remove-choice" data-id="' + model.id_nhanh + '"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event) {
        this.filterData();
    }

    filterChanged(event) {
        this.filterData();
    }

    filterData() {
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.choseproductService.getProduct(page, itemPerpage, filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        )

        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {
            $('.remove-choice').off('click').click((e)=> {
                let id = $(e.target).attr('data-id');
                this.list_product_id.forEach((item, i)=> {
                    if (item.id_nhanh == id) {
                        this.list_product_id.splice(i, 1);
                    }
                })
                this.idChecked.forEach((sid, i)=> {
                    if (sid == id) {
                        this.idChecked.splice(i, 1);
                    }
                })
            })

            $('.input-nhanh').off('change').change((e)=> {
                let idNhanh = $(e.target).attr('id-nhanh');
                let quantity = $(e.target).val();
                this.dataQuantity[idNhanh] = quantity;
                console.log(this.dataQuantity);
            })
        });
    }


    //End config pos-table
    onClickOpenModal() {
        this.filterData();
    }


    ngOnDestroy() {
        this.listenFunc();
        this.globalListenFunc();
    }

    ngAfterViewChecked() {
        if (this.afterView) {
            if ($('.select-product').length > 0) {
                console.log('vao day');
                let that = this;
                $('.select-product').off('click').on('click', function (e) {

                    let id = $(this).attr('data_id');
                    let spid = $(this).attr('sp_id');

                    if ($(this).prop('checked')) {
                        $(this).closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                        that.data.forEach((item)=> {
                            if (item.id_nhanh == spid) {
                                that.list_product_id.push(item);
                            }
                        })
                        that.idChecked.push(spid);
                    } else {
                        $(this).closest('tr').css("background-color", "white");
                        that.list_product_id.forEach((item, i)=> {
                            if (item.id_nhanh == spid) {
                                that.list_product_id.splice(i, 1);
                            }
                        })
                        that.idChecked.forEach((sid, i)=> {
                            if (sid == spid) {
                                that.idChecked.splice(i, 1);
                            }
                        })
                    }
                });
                $('.select-product:checked').closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                this.afterView = false;
            }
        }
    }
    changeComponent = false;


    dataQuantity = [];
    save() {
        let flag = true;
        $('.simple-notification-wrapper').css("z-index",9999999999);
        if(this.idChecked.length > 0){

        }else{
            flag = false;
            this.noti.error(
                'Lỗi!',
                'Phải chọn sản phẩm trước khi lưu'
            );
            return;
        }


        let data = [];
        $('.input-nhanh').each((index,ele)=>{
            let idNhanh = $(ele).attr('id-nhanh');
            let quantity = $(ele).val();
            if(quantity == ''){
                flag = false;
            }
            data.push({id_nhanh:idNhanh,quantity:quantity})
        })

        if(flag){
            this.showLoading = true;
            this.choseproductService.addQuantity(data).subscribe(
                (data)=>{
                    this.showLoading = false;
                }
            );
        }else{
            this.noti.error(
                'Lỗi!',
                'Phải nhập đủ số lượng');
        }

    }

    changedTable(event){

    }
}