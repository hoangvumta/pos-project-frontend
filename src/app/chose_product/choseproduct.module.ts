import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {ChoseProduct} from "./list/list.component";
import {ChoseProductService} from "./choseproduct.service";
import {SimpleNotificationsModule} from "angular2-notifications/src/simple-notifications.module";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {ChoseProduct2} from "./list2/list.component";

import { PosSharedModule } from "../components/PosSharedModule";

export const routes = [
    {path: '', component:ChoseProduct},
    {path: 'list2', component:ChoseProduct2},
];

@NgModule({
    declarations: [
        ChoseProduct,
        ChoseProduct2
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
        SimpleNotificationsModule,
		PosSharedModule
    ],
    providers: [ChoseProductService,NotificationsService]
})
export default class ChoseProductModule {
    static routes = routes;
}
