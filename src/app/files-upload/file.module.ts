import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Files } from './file.component';

export const routes = [
    { path: '', component: Files, pathMatch: 'full' }
];

@NgModule({
    declarations: [
        Files
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
    ]
})
export default class FileModule {
    static routes = routes;
}
