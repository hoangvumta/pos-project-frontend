import { Component, ViewEncapsulation } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {ApiService} from "../helpers/api.service";

@Component({
    selector: 'file',
    styleUrls: [ './file.style.scss' ],
    templateUrl: './file.template.html',
    encapsulation: ViewEncapsulation.None,
    host: {
        class: 'file-page app'
    }
})

export class Files {
    filesToUpload: any;

    constructor(private http: Http, private localStorageService: LocalStorage, private api:ApiService) {
        this.filesToUpload = [];
    }

    fileChangeEvent(fileInput: any){
        this.filesToUpload = fileInput.target.files;
    }
    
    upload() {
        let fileList =  this.filesToUpload;
        this.api.uploadFile(fileList).subscribe(
            (data)=> {
                console.log(data)
            },
            err=>console.log('Error')
        )
    }

}
