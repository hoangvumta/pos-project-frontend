import {Component, Injectable, OnInit, Input} from "@angular/core";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {ProductService} from "../products.service";
import { FormGroup, FormBuilder } from '@angular/forms';
import { Http, URLSearchParams } from '@angular/http';
import {AppConfig} from '../../app.config';

@Component({
    selector: '[tables-basic]',
    templateUrl: './list-products.template.html',
    styleUrls: ['./list-products.style.scss']
})

@Injectable()

export class ListProducts implements OnInit{

    index: number;
    constructor(private router:Router, private productService:ProductService, private formBuilder: FormBuilder, private http: Http, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;
    private datas = [];
    dataFillter: any[];
    filterParam: any;
    fillterForm: FormGroup;
    addElement: any;
    ngOnInit(){
        this.productService.getList().subscribe(
            response => {
                this.datas = response;
            },
            error => {
                console.log(error);
            }
        );
        this.fillterForm = this.formBuilder.group({
            type: 0,
            value: ''
        });
    }

    onChange(event){
        if(event.target.value == 0) {
            this.addElement = false;
            return;
        }
        this.http.post(this.config.baseApiUrl+'/store/get-param', {param: event.target.value}).subscribe(
            res => {
                this.dataFillter = res.json();
                this.fillterForm = this.formBuilder.group({
                    type: this.fillterForm.value.type,
                    value: this.dataFillter[0].name
                });
            }
        );
        this.addElement = true;
        this.filterParam = event.target.value;
        if(event.target.value == 0){
            this.addElement = false;
        }
    }

    onCreate(){
        this.router.navigate(['/products', 'create']);
    }

    onEdit(id){
        this.router.navigate(['/products', id, 'edit']);
    }

    onFillter(form: NgForm){
        let url = this.config.baseApiUrl+'/store/list-store';

        let searchParams = new URLSearchParams();
        searchParams.set(form.value['type'], form.value['value']);

        this.http.get(url, {search: searchParams}).subscribe(
            data => {
                this.datas = data.json();
            }
        )
    }
}