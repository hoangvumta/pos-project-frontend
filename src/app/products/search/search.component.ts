import {Component, OnChanges, OnInit, ElementRef, ViewEncapsulation, ChangeDetectorRef} from '@angular/core';
import {ProductService} from "../products.service";

declare var $: any;
declare var jQuery: any;
@Component({
    selector: '[search]',
    host: {
        '(document:click)': 'handleClick($event)',
    },
    templateUrl: './search.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./search.style.scss']
})
export class Search {
    public query = '';
    public filteredList = [];
    public lists = [];
    public elementRef;
    public message = '';
    public warning = ''
    public data = []

    constructor(myElement: ElementRef, private productService: ProductService) {
        this.elementRef = myElement;
    }

    ngOnInit() {

    }

    filter() {
        this.message = ''
        // $('.suggestions').style.display = 'block'
        if (this.query !== "") {
            this.productService.getInfoProduct(this.query).subscribe(
                response => {
                    this.filteredList = response;
                    console.log(this.filteredList)
                },
                error => {
                    console.log(error);
                }
            );
        } else {
            this.filteredList = [];
        }
    }

    // filterValuePart(arr, part) {
    //     part = part.toString().toLowerCase();
    //
    //     return arr.filter(function(obj) {
    //         return Object.keys(obj)
    //             .some(function(k) {
    //                 return obj[k].toString().toLowerCase().indexOf(part) !== -1;
    //             });
    //     });
    //  };

    select(item) {
        this.query = "";
        if (this.lists.length != 0) {
            let flag = 1;
            for (let i = 0; i < this.lists.length; i++) {
                if (item.id_nhanh == this.lists[i]['id_nhanh']) {
                    flag = 0;
                    this.message = ' Sản phẩm đã có trong bảng'
                }
            }
            if (flag == 1) {
                this.lists.push(item)
                this.data.push({"id":item.id, "quanty" : 0})
            }
        } else {
            this.lists.push(item)
            console.log(this.lists)
            this.data.push({"id":item.id, "quanty" : 0})
        }
        this.filteredList = [];
        console.log(this.filteredList);
    }

    update(event, id) {
        this.warning = ''
        this.message = ''
        if (this.data.length != 0) {
            let flag = 1;
            for (let i = 0; i < this.data.length; i++) {
                if (id == this.data[i]['id']) {
                    flag = 0;
                    this.data[i]['quanty'] = event.target.value
                }
            }
            if (flag == 1) {
                this.data.push({"id": id, "quanty": event.target.value})
            }

        } else {
            this.data.push({"id": id, "quanty": event.target.value})
        }
        console.log(this.data)
    }

    save() {
        this.message = ''
        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i]['quanty'] == "" || this.data[i]['quanty'] == 0) {
                this.warning = 'Số lượng không thể để trống'
            }
        }
        console.log(this.data)
    }

    onDelete(id) {
        this.message = ''
        this.warning = ''
        let index = 0;
        this.lists.forEach((key, val) => {
            if (key.id == id) {
                index = this.lists.indexOf(key)
            }

        })

        this.lists.splice(index, 1)
        console.log(this.lists)

        this.data.forEach((key, val)=> {
            if (key.id == id) {
                this.data.splice(this.data.indexOf(key), 1)
            }
        })
        console.log(this.data)
    }

    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                // inside = true;
            }
             clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
    }

}