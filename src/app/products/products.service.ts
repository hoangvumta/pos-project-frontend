import { Injectable } from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';
@Injectable()

export class ProductService{
    constructor(private http: Http,private localStorageService: LocalStorage , config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;
    public getList(){
        return this.http.get(this.config.baseApiUrl+'/product/get-list-products')
            .map((res) => res.json());
    }

    public getProductDetail(id){
        return this.http.get(this.config.baseApiUrl+'/product/product-detail?id='+id)
            .map((res) => res.json());
    }

    public getInfoProduct(param){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/product/get-all-mj-product?param='+param, {headers:headers})
            .map((res) => res.json());
    }
}