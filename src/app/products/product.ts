export class Product {
  constructor(
      public name: string,
      public description: string,
      public attributes: any,
      public skus: any
  ) {}
}
