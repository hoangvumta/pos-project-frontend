import 'dropzone/dist/dropzone.js';
import { CommonModule } from '@angular/common';
import {FormsModule, FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {Create} from "./create/create.component";
import {Test} from "./test/test.component";
import {Search} from "./search/search.component";
import {ListProducts} from "./list/list-products.component";
import {ProductService} from "./products.service";
import { PopoverModule } from 'ng2-popover';
import { FroalaEditorDirective, FroalaViewDirective } from '../../../node_modules/angular2-froala-wysiwyg/lib/froala.directives';
import {DropzoneDemo} from "../components/dropzone/dropzone.directive";

export const routes = [
	{path: '', component: ListProducts},
    {path: 'test', component: Test},
    {path: 'search', component: Search},
    {path: 'them-moi', component: Create},
    {path: ':id/cap-nhat', component: Create},
];

@NgModule({
    declarations: [
        Create,
        Test,
        Search,
        ListProducts,
		FroalaEditorDirective,
		FroalaViewDirective,
        DropzoneDemo
    ],
    imports: [
        CommonModule,
        FormsModule,
        PopoverModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    providers: [ProductService,FormBuilder]
})
export default class ProductModule {
    static routes = routes;
}
