import {Component, OnChanges, ElementRef, ViewEncapsulation} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {Product} from "../product";
import {ProductService} from "../../helpers/product.service";

import '../../../../node_modules/froala-editor/js/froala_editor.min.js';
import '../../../../node_modules/froala-editor/js/plugins/align.min.js';
import '../../../../node_modules/froala-editor/js/plugins/code_beautifier.min.js';
import '../../../../node_modules/froala-editor/js/plugins/code_view.min.js';
import '../../../../node_modules/froala-editor/js/plugins/draggable.min.js';
import '../../../../node_modules/froala-editor/js/plugins/image.min.js';
import '../../../../node_modules/froala-editor/js/plugins/image_manager.min.js';
import '../../../../node_modules/froala-editor/js/plugins/link.min.js';
import '../../../../node_modules/froala-editor/js/plugins/lists.min.js';
import '../../../../node_modules/froala-editor/js/plugins/paragraph_format.min.js';
import '../../../../node_modules/froala-editor/js/plugins/paragraph_style.min.js';
import '../../../../node_modules/froala-editor/js/plugins/table.min.js';
import '../../../../node_modules/froala-editor/js/plugins/url.min.js';
import '../../../../node_modules/froala-editor/js/plugins/entities.min.js';


declare var $: any;
declare var jQuery: any;
declare var froalaEditor: any;

@Component({
    selector: '[create-pr]',
    templateUrl: './create.template.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./create.style.scss']
})
export class Create{
    productForm: FormGroup;
    productModel: Product;
    myData = 'Size';
    default_attributes = ['Size','Color','vxcvcx'];
    variants = [];
    variants2 = [];
    arrSkus = [];
	hasVariants = false;
	editor: any;

    attributes = [];
    filter_attributes = [];
	weight_type: string = 'kg';
	
	public titleOptions: Object = { 
		placeholderText: 'Edit Your Content Here!',
		imageUploadURL: '/upload_image',
		imageUploadParams: {
			token: 'upload_image_token'
		}
	}


	  // Sample 2 model
	  public content: string = '<span>My Document\'s Title</span>';
    
    //$el: any;
    constructor(private _http:Http,private route: ActivatedRoute,private formBuilder: FormBuilder, private productService: ProductService){

    }

    productId = '';

    ngOnInit(): void {

        this.initForm(false);
        this.route.params.subscribe(
            (params: any) => {
                this.productId = params.id;
            }
        );

        if(this.productId){
            this.productService.getProductDetail(this.productId)
                .subscribe(
                    (data:any)=>{
                        this.productModel = data;
                        this.variants = this.productModel.attributes.attributes_string;
                        this.variants2 = this.productModel.attributes.attributes;
                        this.buildSkus();
                        this.initForm(true);
                    },
                    error => console.log(error)
                )
        } else {
			this.productService.getInitDetail()
                .subscribe(
                    (data:any)=>{
                        console.log(data);
                    },
                    error => console.log(error)
                )
		}

        // this.attributes = ['Màu Sắc','Kích thước','Xuất xứ'];
        // this.variants = [
        //     {name: '1:Size',value: ['3123:38','12321:39','434:40']},
        //     {name: '2:Color',value: ['4123:red','5534543:black']},
        //     {name: '3:KT',value: ['425234:ccc','5346435:xxx','4234324:eee']}
        // ];

        this.buildSkus();


    }

    oldLength = 0;
    ngAfterViewInit(){
        this.oldLength = this.variants.length;
        $('.pos-tag').click(function () {
            $(this).find('.pos-tag-input').focus();
        });
    }

    ngAfterViewChecked(){
        if(this.variants.length != this.oldLength){
            this.oldLength = this.variants.length;
            $('.pos-tag').click(function () {
                $(this).find('.pos-tag-input').focus();
            });
        }
    }
	//#region Script for Variants
	toggleVariants() {
		this.hasVariants = !this.hasVariants;
	}
	
	removeVariant(index) {
        this.variants.splice(index,1);
        this.variants2.splice(index,1);
        this.buildSkus();
    }

    addVariant(){
        this.variants.push({name:'',value:[]});
        this.variants2.push({name:'',value:[]});
    }
	//#endregion
	
	change_weight_type(type) {
		console.log(type);
		this.weight_type = type;
	}

    getValueAttributeOption(val){
        let id = val.split(":")[0];
        return val.replace(id+':','');
    }

    getIdAtrributeOption(val){
        return val.split(":")[0];
    }
	
	
	onTagInputChange(event, i) {
		if(event.target.value != ''){
			this.addTag(event.target.value, event.target, i);
		}
	}
	
	onTagInputKeyDown(event, i) {
		if(event.keyCode == 13 || event.keyCode == 188 || event.keyCode == 9) {
			let tag = event.target.value;
			if(tag == '')
				return;
			
			let lastChar = tag.substr(tag.length - 1);
			if(lastChar == ',') {
				tag = tag.substr(0,tag.length - 1);
			}
			this.addTag(tag, event.target, i);
			event.preventDefault();
		}
	}
	
	forcusTagInput(event) {
		let el = event.target;
		console.log('here');
		console.log(el);
		$(el).find('.tag-input').focus();
	}
	
	isBigEnough(value) {
		return value.indexOf("")
	}

    onChangeAttributeName(event,i){
		let text = event.target.value;
		let filtered = this.default_attributes.filter(function (el, index, arr) {
			let s
			return String(el).toLowerCase().indexOf(String(text).toLowerCase()) >= 0;
		});
		
		console.log(filtered);
        this.variants[i].name = event.target.value;
        this.variants2[i].name = event.target.value;
        this.buildSkus();
    }

    addTag(tag, element,i){
		if(element.value == '')
			return;
		let oldValue = [];
		let oldValue2 = [];
		let value = tag;
		oldValue = this.variants[i].value;
		oldValue2 = this.variants2[i].value;

		var flag = true;
		for (var item of oldValue) {
			if(String(item).toLowerCase() == String(value).toLowerCase()){
				flag = false;
			}
		}

		if(flag) {
			oldValue.push(this.variants[i].name + ':' + value);
			oldValue2.push(this.variants2[i].name + ':' + value);
			element.value = '';
			$(element).attr('data-original-title','');
			$(element).tooltip("hide");
		} else {
			$(element).attr('data-original-title','Đã có');
			//$(event.target).tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');
			$(element).tooltip('show');
		}
        this.buildSkus();
    }

    removeTag(name,i) {
        let index = this.variants[i].value.indexOf(name);
        this.variants[i].value.splice(index,1);
        this.variants2[i].value.splice(index,1);
        this.buildSkus();
    }

    save(){
        this.productForm.controls['attributes'].setValue({
            attributes_string: this.variants,
            attributes: this.variants2
        });
    }


    buildSkus() {
        this.arrSkus = [];
        var arr = [];
        for (var item of this.variants) {
            arr.push(item.value);
        }

        var arr2 = [];
        for (var item of this.variants2) {
            arr2.push(item.value);
        }

        function recursiveSku(arr) {
            if (arr.length >= 2) {
                var result = [];
                for (var i1 of arr[0]) {
                    for (var i2 of arr[1]) {
                        let x = i1 + ';' + i2;
                        result.push(x);
                    }
                }
                arr.splice(0, 2);
                arr.unshift(result);
            } else {
                return false;
            }
            recursiveSku(arr);
        }

        recursiveSku(arr);
        recursiveSku(arr2);

        if (arr.length > 0) {
            arr[0].forEach((skuString, index) => {
                let skus = arr2[0][index];
                let obj = {checked: 1, sku_string: skuString,skus:skus,price: 0, barcode: ''};
                this.arrSkus.push(obj);
            });
        }


        var f = false;
        var rowspans = [];
        var r = 1;
        this.variants.reverse().forEach((v,i) => {
            r = r*v.value.length;
            rowspans.push(r);
        });
        rowspans = rowspans.reverse().slice(1,rowspans.length);
        rowspans.push(1);
        this.variants.reverse();

        this.arrSkus.forEach((item,index2) => {
            let td = [];
            var arrDisplay = [item.sku_string];
            if(String(item.sku_string).indexOf(';') != -1){
                arrDisplay = item.sku_string.split(';');
            }
            rowspans.forEach((r,i) => {
                if(index2%r == 0){
                    let objTd = {rowspan:r,display:this.getValueAttributeOption(arrDisplay[i])}
                    td.push(objTd);
                }
            })
            this.arrSkus[index2].td = td;
        });

    }

    initForm(edited:boolean) {
        //Form Products
        if(edited){
            this.productForm = this.formBuilder.group({
                name: [this.productModel.name, Validators.required],
                description: [this.productModel.description],
                attributes: [this.productModel.skus]
            });
        }else{
            this.productForm = this.formBuilder.group({
                name: ['', Validators.required],
                description: [''],
                attributes: []
            });
        }
    }
}