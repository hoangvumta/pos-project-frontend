export class BillWareHouse {
    constructor(
        public id: number = null,
        public warehouse_from_id: number = 0,
        public warehouse_to_id: number = 0,
        public user_id: number = 0,
        public notes: string = null,
        public transport_time: string = null,
        public type: number = 2

    ) {}
}