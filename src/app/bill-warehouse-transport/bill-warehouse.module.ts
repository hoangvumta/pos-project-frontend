import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {BillWarehouse} from "./create/create.component";
import {BillList} from "./list/list.component";
import {BillWarehouseService} from "./bill-warehouse.service";
import {WareHouseService} from "../warehouse/warehouse.service";
import {SimpleNotificationsModule} from "angular2-notifications/src/simple-notifications.module";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {ValidationService} from "../components/validation/validation.service";

import { PosSharedModule } from "../components/PosSharedModule";
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import 'ng2-datetime/src/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js';

export const routes = [
    {path: 'them-moi', component:BillWarehouse},
    {path: ':id/cap-nhat', component:BillWarehouse},
    {path: '', component:BillList}
];


@NgModule({
    declarations: [
        BillWarehouse,
        BillList
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
        SimpleNotificationsModule,
		PosSharedModule,
        NKDatetimeModule

    ],
    providers: [BillWarehouseService,NotificationsService, WareHouseService, ValidationService]
})
export default class BillWarehouseModule {
    static routes = routes;
}
