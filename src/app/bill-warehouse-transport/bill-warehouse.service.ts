import { Injectable } from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {BillWareHouse} from "./bill-warehouse";
import {Observable} from "rxjs/Rx";
import {AppConfig} from '../app.config';

@Injectable()

export class BillWarehouseService{
    constructor(private http: Http, private localStorageService: LocalStorage, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;


    getProduct(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/product/get-product-moji?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }
    
    getWarehouse(){
         let token = this.localStorageService.get('token');
         let headers = new Headers();
         headers.append('Authorization', `Bearer ${token}`);
         headers.append('Content-Type','application/x-www-form-urlencoded');
         return this.http.get(this.config.baseApiUrl+'/warehouse/get-all-warehouse', {headers:headers})
             .map(res => res.json())
     }

    getSupplier(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/utility/get-supplier', {headers:headers})
            .map(res => res.json())
    }
    getUser(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/utility/get-all-user', {headers:headers})
            .map(res => res.json())
    }
    public create(info: BillWareHouse, products): Observable<boolean> {
        let body = {info, products:products};
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.post(this.config.baseApiUrl+'/bill/create', JSON.stringify(body),{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public edit(info: BillWareHouse, id, products): Observable<boolean> {
        let body = {info, products:products };
        info.id = id;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.post(this.config.baseApiUrl+'/bill/update', JSON.stringify(body),{
            headers: headers
        }).map((response: Response) => response.json());
    }
    public getCouponDetail(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/bill/view?id='+id, {headers:headers})
            .map((res) => res.json());
    }
    public getInfoProduct(param){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/product/get-all-mj-product?param='+param, {headers:headers})
            .map((res) => res.json());
    }

    getBill(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/bill/get-list-bill?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }

    deleteBill(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/bill/delete?id='+id,{headers:headers})
            .map(res=>res.json())
    }

    getCompany(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/v1/warehouse/get-list-company', {headers:headers})
            .map(res => res.json())
    }
    
    verifyBill(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/bill/verify-bill?id='+id, {headers:headers})
            .map(res => res.json())
    }
}