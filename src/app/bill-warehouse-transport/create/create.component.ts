import {Component, Injectable, OnInit, Renderer, ElementRef, ViewChild, ViewEncapsulation} from "@angular/core";
import {UrlHelper} from "../../helpers/UrlHelper";
import { BillWareHouse} from "../bill-warehouse.ts";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {WareHouseService} from "../../warehouse/warehouse.service";
import {BillWarehouseService} from "../bill-warehouse.service";
import {Subscription} from "rxjs/Rx";
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { Router, ActivatedRoute} from "@angular/router";
declare var $:any;
@Component({
    selector: '[tables-basic]',
    host: {
        '(document:click)': 'handleClick($event)',
    },
    templateUrl: './create.template.html',
    styleUrls: ['./create.style.scss'],
    encapsulation: ViewEncapsulation.None
})

@Injectable()
export class BillWarehouse implements OnInit {
    @ViewChild('selector') selector: ElementRef;
    model = new BillWareHouse();
    listenFunc:Function;
    showLoading = false;
    afterView = false;
    exampleWarehouse = [];
    exampleUser = [];

    list_product_id = [];
    idChecked = [];
    changeComponent = false;

    differ:any;
    couponId = null;
    verify_status = null;
    private subscription: Subscription;
    couponForm: FormGroup;

    //Search Product
    public query = '';
    public filteredList = [];
    public message = '';
    public warning = '';


    constructor(
        private elementRef:ElementRef,
        private renderer:Renderer,
        private noti: NotificationsService,
        private warehouseService: WareHouseService,
        private couponService: BillWarehouseService,
        private location:Location,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.elementRef = elementRef;
    }
    

    //Notification
    optionsNoti = {
        position: ["bottom", "right"],
        timeOut: 1000,
    };

    ngOnInit():void {
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if(params.hasOwnProperty('id')) {
                    this.couponId = params.id;
                }else {
                    this.couponId = null;
                }
                this.initForm();
            }
        );
        
        this.couponService.getWarehouse().subscribe(
            res => {
                res.unshift({id: 0, text: 'Kho hàng'});
                this.exampleWarehouse = res;
            }
        );
        this.couponService.getUser().subscribe(
            res => {
                res.unshift({id: 0, text: 'Nhân viên'});
                this.exampleUser = res;
            }
        );
    }

    validateNumber(){
        $('#datepicker1').keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

    
    filter() {
        this.message = '';
        if (this.query !== "") {
            this.couponService.getInfoProduct(this.query).subscribe(
                response => {
                    this.filteredList = response;
                },
                error => {
                    console.log(error);
                }
            );
        } else {
            this.filteredList = [];
        }
    }

    select(item) {
        this.query = "";
        if (this.list_product_id.length != 0) {
            let flag = 1;
            for (let i = 0; i < this.list_product_id.length; i++) {
                if (item.id_nhanh == this.list_product_id[i]['id_nhanh']) {
                    flag = 0;
                    // this.message = ' Sản phẩm đã có trong bảng'
                }
            }
            if (flag == 1) {
                this.list_product_id.push(item);
            }
        } else {
            this.list_product_id.push(item);
        }
        this.filteredList = [];
    }

    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
    }

    initForm() {
        //Form Products
        if(this.couponId != null){
            this.couponService.getCouponDetail(this.couponId).subscribe(
                (data:any)=>{
                    if(data.info == null){
                        this.router.navigate(['/error']);
                    }
                    else {
                        this.couponForm = this.formBuilder.group({
                            warehouse_from_id: ['', Validators.required],
                            warehouse_to_id: ['', Validators.required],
                            notes: new FormControl(),
                            transport_time: new FormControl(),
                            final_price: new FormControl(),
                            search : new FormControl()
                        });
                        this.model = data.info;
                        this.model.warehouse_to_id = data.info.to;
                        this.model.warehouse_from_id = data.info.from;
                        this.verify_status = data.info.verify_status;
                        this.couponService.getWarehouse().subscribe(
                            res => {
                                this.exampleWarehouse = res
                            }
                        );

                        this.list_product_id = data.products;
                        this.list_product_id.forEach((item, index)=> {
                            this.idChecked.push(item.id);
                        });

                        this.filterData();
                        this.changeComponent = true;
                    }

                },
                (error)=>console.log(error)
            )

        }
        else{
            this.couponForm = this.formBuilder.group({
                warehouse_from_id: [this.model.warehouse_from_id, Validators.required],
                warehouse_to_id: [this.model.warehouse_to_id, Validators.required],
                notes: new FormControl(),
                transport_time: new FormControl(),
                search : new FormControl()
            });
        }

    }

    onSubmit(couponForm){
        if(this.couponId){
            this.showLoading = true;
            this.model.transport_time = $($('#datepicker1').find('input')[0]).val();
            if(this.model.warehouse_to_id == 0 || this.model.warehouse_from_id == 0 || this.model.user_id == 0 || this.list_product_id.length == 0 ){
                this.message = 'Vui lòng điền đủ thông tin!';
                this.warning = 'Chưa có thông tin sản phẩm !Vui lòng chọn sản phẩm!';
                return false ;
            }
            this.couponService.edit(this.model,this.couponId, this.list_product_id).subscribe(
                (data:any) =>{
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Lưu thành công'
                        );
                    }else{
                        for (let propertyName in data.errors) {
                            this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.showLoading = false
            )
        }else{
            this.showLoading = true;
            this.model.transport_time = $($('#datepicker1').find('input')[0]).val();
            if( this.model.warehouse_to_id == 0 || this.model.warehouse_from_id == 0 || this.model.user_id == 0 || this.list_product_id.length == 0 ){
                this.message = 'Vui lòng điền đủ thông tin!';
                this.warning = 'Chưa có thông tin sản phẩm !Vui lòng chọn sản phẩm!';
                return false ;
            }

            this.couponService.create(this.model, this.list_product_id).subscribe(
                (data:any) =>{
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Đã thêm phiếu mới'
                        );
                        this.couponId = data.id;
                        this.location.replaceState("/phieu-chuyen-kho/"+this.couponId+"/cap-nhat");
                    }else{
                        for (let propertyName in data.errors) {
                            this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.showLoading = false
            )
        }
    }


    //Config pos-table
    data = [];
    settings = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: '',
                value: (model, index)=> {
                    let id = model.id;
                    let checked = '';

                    if (this.idChecked.indexOf(id) > -1) {
                        checked = 'checked';
                    } else {
                        checked = '';
                    }
                    let html = '<label>' +
                        '<input class="select-product" sp_id="' + id + '" data_id="' + index + '" type="checkbox" ' + checked + '>' +
                        '</label>';
                    return html;
                },
                format: 'raw'
            },
            {
                label: 'ID Nhanh',
                value: (model)=> {
                    return model.id_nhanh;
                },
                sort: {attribute: 'id_nhanh'},
                filter: {
                    attribute: 'id_nhanh',
                    type: 'input'
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                },
                sort: {attribute: 'sku'},
                filter: {
                    attribute: 'sku',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            }
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    };

    settings2 = {
        columns: [
            {
                label: '#',
                value: (model, index)=> {
                    return index + 1;
                }
            },
            {
                label: 'ID',
                value: (model)=> {
                    return model.id;
                }
            },
            {
                label: 'Mã sản phẩm',
                value: (model)=> {
                    return model.sku;
                }
            },
            {
                label: 'Tên',
                value: (model)=> {
                    return model.name;
                }
            },
           
            {
                label: 'Số lượng',
                value: (model)=> {
                    let val = '';
                    if(model.quantity){
                        val = model.quantity;
                    }
                    let html = '<input type="text" class="form-control select-quantity" data-id="' + model.id + '" value="'+val+'" />';
                    return html;
                },
                format: 'raw'
            },

            {
                label: '',
                value: (model)=> {
                    let html = '<a class="glyphicon glyphicon-trash remove-choice" style="margin-top: 10px" data-id="' + model.id_nhanh + '"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    };

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event) {
        this.filterData();
    }

    filterChanged(event) {
        this.filterData();
    }

    onVerify(id){
        this.couponService.verifyBill(id).subscribe(
            (data:any) =>{
                if(data.success == 1){
                    this.noti.success(
                        'Thành Công!',
                        'Đã duyệt phiếu'
                    );
                    this.router.navigate(['/phieu-chuyen-kho']);
                }else{
                    for (let propertyName in data.errors) {
                        this.couponForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                    }
                }
            },
            err => console.log(err),
            () => this.showLoading = false
        )
    }

    filterData() {
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.couponService.getProduct(page, itemPerpage, filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        );
        let that = this;
        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {

            $('.remove-choice').off('click').click((e)=> {
                let id = $(e.target).attr('data-id');
                this.list_product_id.forEach((item, i)=> {
                    if (item.id_nhanh == id) {
                        this.list_product_id.splice(i, 1);
                    }
                });
                this.idChecked.forEach((sid, i)=> {
                    if (sid == id) {
                        this.idChecked.splice(i, 1);
                    }
                });
            });


            // textbox is number
            $(' .select-quantity ').keydown(function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });


            $('.select-quantity').off('change').change((e)=> {
                let id = $(e.target).attr('data-id');
                let quantity = $(e.target).val();
                this.list_product_id.forEach((item, index) => {
                    if(item.id == id){
                        this.list_product_id[index].quantity = quantity;
                    }
                });
            });

        });
    }



    //End config pos-table
    onClickOpenModal() {
        this.filterData();
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private afterSelectFilterRender = true;
    ngAfterViewChecked() {
        if($('.filter-select option').length > 0 && this.afterSelectFilterRender){
            $('.filter-select').select2({
                theme: 'bootstrap',
                width: '100%'
            });
            $('.filter-select').change((event)=>{

                if($(event.target).attr('name') == 'warehouse_from_id'){
                    this.model.warehouse_from_id = event.target.value;
                }
                if($(event.target).attr('name') == 'warehouse_to_id'){
                    this.model.warehouse_to_id = event.target.value;
                }
                if($(event.target).attr('name') == 'user_id'){
                    this.model.user_id = event.target.value;
                }
            });
            this.afterSelectFilterRender =false;
        }

        if (this.afterView) {
            if ($('.select-product').length > 0) {
                let that = this;
                $('.select-product').off('click').on('click', function (e) {

                    let id = $(this).attr('data_id');
                    let spid = $(this).attr('sp_id');

                    if ($(this).prop('checked')) {
                        $(this).closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                        that.data.forEach((item, index)=> {
                            if (item.id == spid) {
                                that.list_product_id.push(item);
                            }
                        });
                        that.idChecked.push(spid);
                    } else {
                        $(this).closest('tr').css("background-color", "white");
                        that.list_product_id.forEach((item, i)=> {
                            if (item.id == spid) {
                                that.list_product_id.splice(i, 1);
                            }
                        });
                        that.idChecked.forEach((sid, i)=> {
                            if (sid == spid) {
                                that.idChecked.splice(i, 1);
                            }
                        })
                    }
                });
                $('.select-product:checked').closest('tr').css("background-color", "rgba(0, 128, 0, 0.28)");
                this.afterView = false;
            }
        }

    }

}