import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';
@Injectable()

export class EmployeeService{
    constructor(private http: Http, private localStorageService: LocalStorage, config: AppConfig ){
        this.config = config.getConfig();
    }
    config: any;


    getEmployee(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/employee/get-list-employee?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }

    createEmployee(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/employee/create',body,{headers:headers})
            .map(res=>res.json())
    }

    editEmployee(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/employee/update',body,{headers:headers})
            .map(res=>res.json())
    }

    deleteEmployee(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/employee/delete?id='+id,{headers:headers})
            .map(res=>res.json())
    }

    detailEmployee(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/employee/view?id='+id,{headers:headers})
            .map(res=>res.json())
    }

}