import {Component, OnInit } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/Rx';
import { Employees } from '../employees';
import {Router} from "@angular/router";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {EmployeeService} from "../employees.service";

@Component({
    selector: '[employee]',
    templateUrl: './employee.template.html',
    styleUrls: ['./employee.style.scss'],
})
export class Employee implements OnInit{
	selected: any;

    selectedValues = [];

    selectedProvince: any;
    selectedDistrict: any;
    selectedWard: any;

    message =null;
    warning ='';
    danger ='';
    model = new Employees();
    submitted = false;

    complexForm : FormGroup;
    loading = false;
    

    constructor(private http: Http, private employeeService:EmployeeService,private router: Router){
        this.complexForm = new FormGroup({
            'username' : new FormControl('',Validators.required ),
            'password' : new FormControl('',[Validators.required , Validators.minLength(6)] ),
            'repassword' : new FormControl('',[Validators.required , Validators.minLength(6)]),
            'email' : new FormControl('',[Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")] ),
            'phone' : new FormControl('',[
                Validators.required,
                validatePhone
            ]),
            'address' : new FormControl('',Validators.required ),
            'name' : new FormControl('',Validators.required ),
            // 'role_id' : new FormControl('',Validators.required ),
        });

        function validatePhone($att:FormControl) {
            $att = $att.value;
            if($att == null || typeof $att == "undefined") return {phoneValid:false};
            let flag = true;
            let phone = $att.toString();
            //Bắt đầu bằng số 0
            if(parseInt(phone.slice(0,1)) != 0){
                flag = false;
            }
            //Đầu số phải là 09 or 01
            if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
                flag=false;
            }
            //Đầu 09 độ dài = 10
            if(phone.slice(0,2) == '09'){
                if(phone.length != 10) {
                    flag = false;
                }
            }

            //Đầu 01 độ dài 11
            if(phone.slice(0,2) == '01'){
                if(phone.length != 11) flag = false;
            }

            //Độ dài 10-11
            if (phone.length < 10 || phone.length > 11) flag = false;

            if(flag == false){

                return {phoneValid:false};
                // this.message = 'Số điện thoại không đúng định dạng';
            }else{
                return null;
            }
        }
    }

    ngOnInit() {}


    onChangeSelectProvince($event){
        this.model.province_id = $event.value;
    }

    onChangeSelectDistrict($event){
        this.model.district_id = $event.value;
    }

    onChangeSelectWard($event){
        this.model.ward_id = $event.value;
    }
    change(options) {
        this.selectedValues = Array.apply(null,options)
            .filter(option => option.selected)
            .map(option => option.value);
    }
    


    submitForm(complexForm){
        this.loading = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0){
            this.message = 'Vui lòng điền đủ thông tin!';
            return false ;
        }
        if(this.model.password_hash != this.model.password_re){
            this.message = 'Xác nhận lại mật khẩu';
            return false;
        }

        this.model.role_id = this.selectedValues;

        this.employeeService.createEmployee(this.model).subscribe(
            res => {
                this.router.navigate(['/nhan-vien']);
            },
            err => console.log(err),
            () => this.loading = false
        );
    }


}