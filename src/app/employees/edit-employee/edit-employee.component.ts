import {Component, Injectable, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { Employees} from "../employees";
import {EmployeeService} from "../employees.service";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {Subscription} from "rxjs/Rx";
@Component({
    selector: 'edit',
    templateUrl: './edit-employee.template.html',
    styleUrls: ['./edit-employee.style.scss'],
})

@Injectable()

export class EditEmployee implements OnInit{
    selected: any;
    idProvince =0;
    idDistrict = 0;
    idWard =0;

    employeeId = null;
    private subscription: Subscription;

    selectedValues:any;

    message =null;
    warning ='';
    danger ='';
    model = new Employees();
    submitted = false;

    complexForm : FormGroup;
    loading = false;

    constructor(private employeeService:EmployeeService,private router: Router, private route: ActivatedRoute){
    }
    
    ngOnInit(){
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if(params.hasOwnProperty('id')) {
                    this.employeeId = params.id;
                }else {
                    this.employeeId = null;
                }
                this.initForm();
            }
        );
    }
    
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    initForm() {
        if(this.employeeId != null){
            this.employeeService.detailEmployee(this.employeeId).subscribe(
                (data:any)=>{
                    this.complexForm = new FormGroup({
                        'id' : new FormControl('', Validators.required),
                        'username' : new FormControl('',Validators.required ),
                        'password' : new FormControl('',[Validators.required , Validators.minLength(6)] ),
                        'repassword' : new FormControl('',[Validators.required , Validators.minLength(6)] ),
                        'email' : new FormControl('',[Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")] ),
                        'phone' : new FormControl('',[
                            Validators.required,
                            this.validatePhone
                        ]),
                        'address' : new FormControl('',Validators.required ),
                        'name' : new FormControl('',Validators.required ),
                    });

                    this.model.id = data.id;
                    this.model.username = data.username;
                    this.model.password_hash = data.password_hash;
                    this.model.name = data.name;
                    this.model.email = data.email;
                    this.model.phone = data.phone;
                    this.model.address = data.address;
                    this.model.role_id = data.role_id;
                    
                    this.idProvince = data.province_id;
                    this.idDistrict = data.district_id;
                    this.idWard = data.ward_id;
                },
                (error)=>console.log(error)
            )

        }
        // else{
        //     this.complexForm = this.formBuilder.group({
        //         name: ['', Validators.required],
        //         province_id: [this.idProvince,Validators.required],
        //         district_id: [this.idDistrict,Validators.required],
        //         ward_id: [this.idWard,Validators.required],
        //         description: ['',Validators.required],
        //         address: ['',Validators.required],
        //         phone: ['',Validators.required],
        //         id: ['',Validators.required],
        //         type: ['',Validators.required]
        //     });
        // }
    }

    validatePhone($att:FormControl) {
        $att = $att.value;
        if($att == null || typeof $att == "undefined") return {phoneValid:false};
        let flag = true;
        let phone = $att.toString();
        //Bắt đầu bằng số 0
        if(parseInt(phone.slice(0,1)) != 0){
            flag = false;
        }
        //Đầu số phải là 09 or 01
        if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
            flag=false;
        }
        //Đầu 09 độ dài = 10
        if(phone.slice(0,2) == '09'){
            if(phone.length != 10) {
                flag = false;
            }
        }

        //Đầu 01 độ dài 11
        if(phone.slice(0,2) == '01'){
            if(phone.length != 11) flag = false;
        }

        //Độ dài 10-11
        if (phone.length < 10 || phone.length > 11) flag = false;

        if(flag == false){

            return {phoneValid:false}
            // this.message = 'Số điện thoại không đúng định dạng';
        }else{
            return null;
        }
    }

    selectProvince($event){
        if($event.value==0) {
            this.model.province_id = null;
        }else {
            this.model.province_id = $event.value;
        }

    }
    selectDistrict($event){
        if($event.value==0) {
            this.model.district_id = null;
        }else {
            this.model.district_id = $event.value;
        }
    }
    selectTown($event){
        if($event.value==0) {
            this.model.ward_id = null;
        }else {
            this.model.ward_id = $event.value;
        }
    }
    
    change(options) {
        this.selectedValues = Array.apply(null,options)
            .filter(option => option.selected)
            .map(option => option.value);
    }


    editEmployee(complexForm){
        this.loading = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0 ){
            this.message = 'Vui lòng điền đủ thông tin';
            return false ;
        }
        this.model.role_id = this.selectedValues;
        this.employeeService.editEmployee(this.model).subscribe(
            res => {
                this.router.navigate(['/nhan-vien']);

            },
            err => console.log(err),
            () => this.loading = false
        );
    }
}