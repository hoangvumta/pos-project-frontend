export class Employees {
    constructor(
        public id: number =null,
        public username: string = null,
        public name: string = null,
        public password_hash: string = null,
        public password_re: string = null,
        public email: string = null,
        public address: string = null,
        public phone: string = null,
        public province_id: number = null,
        public district_id: number = null,
        public ward_id: number = null,
        // public token: number = null,
        public role_id:any = null
    ) {  }
}