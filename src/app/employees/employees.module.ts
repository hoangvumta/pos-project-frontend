import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {Employee} from "./create/employee.component.ts";
import {ListEmployee} from "./list-employee/list-employee.component.ts";
import {DepDrop} from "./../components/depdrop/depdrop.component.ts";
import {EditEmployee} from "./edit-employee/edit-employee.component";
import { PopoverModule } from 'ng2-popover';
import {EmployeeService} from "./employees.service";
import {PostTable} from "../components/pos-table/pos-table.component";
import {HtmlOutlet} from "../helpers/html-outlet";
import {PaginationModule} from "ng2-bootstrap/ng2-bootstrap";

export const routes = [
    {path: '', redirectTo: 'create', pathMatch: 'full'},
    {path: 'them-moi', component: Employee},
    {path: '', component:ListEmployee},
    {path: ':id/cap-nhat', component: EditEmployee},
];

@NgModule({
    declarations: [
        Employee,
        ListEmployee,
        DepDrop,
        EditEmployee,
        PostTable,
        HtmlOutlet

    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PopoverModule,
        PaginationModule
    ],
    providers: [EmployeeService]
})
export default class EmployeesModule {
    static routes = routes;
}
