import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepDrop } from "./depdrop/depdrop.component";
import { ControlMessages } from "./validation/control-messages.component";
import {PaginationModule} from "ng2-bootstrap/ng2-bootstrap";

import {PostTable} from "./pos-table/pos-table.component";
import {HtmlOutlet} from "../helpers/html-outlet";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		DepDrop,
		ControlMessages,
		PostTable,
        HtmlOutlet
	],
	imports: [
		CommonModule,
		PaginationModule
	],
	exports: [
		DepDrop,
		ControlMessages,
		PostTable
	]
})
export class PosSharedModule {}