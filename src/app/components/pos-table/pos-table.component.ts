import {
    Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ElementRef, ViewChild,
    IterableDiffers
} from "@angular/core";
import {isObject} from "rxjs/util/isObject";
import {UrlHelper} from "../../helpers/UrlHelper";

declare var $:any;
@Component({
    selector: 'pos-table',
    templateUrl: './pos-table.template.html',
    styleUrls: ['./pos-table.style.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class PostTable implements OnInit {
    @ViewChild('selector') selector: ElementRef;

    @Input() settings;
    @Input() data;
    @Input() changeComponent;
    @Output() pageChanged = new EventEmitter();
    @Output() sortChanged = new EventEmitter();
    @Output() filterChanged = new EventEmitter();
    @Output() changedTable = new EventEmitter();

    columns;
    rows;
    th;
    rowsFilter;

    //pagination
    public totalItems:number = 0;
    public currentPage:number;
    public maxSize:number = 5;
    public itemsPerPage = 8;


    ngAfterViewInit() {


    }
    previousInputSettings;
    differ:any;
    ngDoCheck() {
        var changes = this.differ.diff(this.data);

        if (changes) {
                this.totalItems = this.settings.pagination.totalItems;
                this.currentPage = parseInt(this.settings.pagination.currentPage);
            this.previousInputSettings = this.changeComponent;
                this.refreshTable();
        }
        if(this.changeComponent == this.previousInputSettings) {

        }else{
            this.refreshTable();
            this.previousInputSettings = this.changeComponent;
        }

    }

    public setPage(pageNo:number):void {
        this.currentPage = pageNo;
    };

    public changePage(event:any):void {
        if(this.settings.pagination.currentPage != event.page || this.settings.pagination.currentPage == 1) {
            let page = event.page;
            UrlHelper.insertParam('page',page);
            this.pageChanged.emit(event);
        }
    };

    public changeFilter(event:any,ele?):void {
        let attr = event.target.attributes.name.value;
        let value = event.target.value;
        UrlHelper.insertParam(attr,value);
        UrlHelper.insertParam('page',1);
        this.filterChanged.emit({attribute:attr,value:value});
    };
    //End

    constructor(differs: IterableDiffers) {
        this.differ = differs.find([]).create(null);
    }

    ngOnInit() {
        this.itemsPerPage = this.settings.pagination.itemsPerPage;
        this.currentPage = parseInt(this.settings.pagination.currentPage);
        this.refreshTable();
    }
    ngOnChanges(changes){
        if(isObject(changes.data)){

        }
    }

    changeSort(event){
        let currentSort = UrlHelper.getParams()['sort'];
        if(!currentSort){
            UrlHelper.insertParam('sort',event);
        }else{
            if(currentSort.split('')[0]=='-'){
                UrlHelper.insertParam('sort',event);
            }else{
                UrlHelper.insertParam('sort','-'+event);
            }
        }
        let sort = UrlHelper.getParams()['sort'];
        this.sortChanged.emit(event);
    }

    refreshTable(){
        this.columns = [];
        this.rows = [];
        this.th = [];
        this.rowsFilter = [];

        this.columns = this.settings.columns;
        this.columns.forEach(lb=>{
            let sort = (lb.sort && lb.sort.attribute)?lb.sort:false;

            let currentSort = UrlHelper.getParams()['sort'];
            let order = '';
            if(!currentSort){
                order = '';
            }else{
                if(sort && (currentSort == sort.attribute || currentSort == '-'+sort.attribute)){
                    if(currentSort.split('')[0]=='-'){
                        order = 'desc';
                    }else{
                        order = 'asc';
                    }
                }
            }
            let obj = {label:lb.label,sort:sort,order:order, style:lb.style};

            this.th.push(obj);

            //filter
            if(lb.hasOwnProperty('filter')) {
                let attr = lb.filter.attribute;
                let type = lb.filter.type;
                let data = '';
                if(type == 'dropdown'){
                    data = lb.filter.data();
                }

                let value = (UrlHelper.getParams()[attr])?UrlHelper.getParams()[attr]:'';
                this.rowsFilter.push({attribute:attr,type:type,value:value,data:data});
            }else{
                this.rowsFilter.push(false);
            }
        });

        this.data.forEach( (model,index)=>{
            let row = [];
            let currPage = 1;
            if(this.currentPage){
                currPage = this.currentPage;
            }
            this.columns.forEach(col=>{
                let rowVal = col.value(model,index+(currPage-1)*this.itemsPerPage);
                row.push({value:String(rowVal),format:(col.format)?col.format:false, style:col.style});
            })
            this.rows.push(row);
        })
        this.afterSelectFilterRender = true;

        this.changedTable.emit(true);
    }

    private afterSelectFilterRender = true;
    ngAfterViewChecked() {
        if($('.filter-select option').length > 0 && this.afterSelectFilterRender){
            $(this.selector.nativeElement).find('select').select2({
                theme: 'bootstrap',
                width: '100%'
            });
            $(this.selector.nativeElement).find('select').change((event)=>{
                let attr = event.target.attributes.name.value;
                let value = event.target.value;
                UrlHelper.insertParam(attr,value);
                UrlHelper.insertParam('page',1);
                this.filterChanged.emit({attribute:attr,value:value});
            })
            this.afterSelectFilterRender =false;
        }
    }
}