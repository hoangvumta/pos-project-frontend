import {Component, Input, Output, EventEmitter, OnChanges, ViewEncapsulation, SimpleChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy} from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
import { LocalStorage } from '../../helpers/localstorage.service';
import {forEach} from "@angular/router/src/utils/collection";
declare var jQuery:any;
@Component({
    selector: 'pos-dropdown',
    template: '<select #selector id="{{this.selectId}}"></select>',
	encapsulation: ViewEncapsulation.None,
	styleUrls: ['./depdrop.style.scss']
})

export class DepDrop implements OnChanges{
    @ViewChild('selector') selector: ElementRef;

    // data for select2 dropdown
    @Input() value: any;
	@Input() linkData: string;
	@Input() selectId: string;
	@Input() selected: any;
	@Input() parentId: string;

    @Output() valueChanged = new EventEmitter();
    @Output() blur = new EventEmitter();

    // Optional options for select2
    @Input() width: string;
    @Input() theme: string;
	@Input() placeholder: string;
	@Input() firstSelectOption;
	@Input() validateForm;

    private element: any;
	private parentElement: any;
	private data: any;

	constructor(private http: Http, private localStorageService: LocalStorage){}

    ngOnChanges(changes: SimpleChanges) {
        if(this.element && changes['selected'] && changes['selected'].previousValue != changes['selected'].currentValue) {
        	 // console.log(changes)
            this.initPlugin(null);
        }
    }

    ngAfterViewInit() {
        let that = this;

        this.element = jQuery(this.selector.nativeElement);

		if(this.parentId) {
			this.parentElement = jQuery('#' + this.parentId);
			
			let that = this;
			this.parentElement.on("change.select2", function (e) {
				let parentValue = that.parentElement.find('select').val();
				let currentValue = that.element.val();
				that.initPlugin(parentValue);
			});
			
			let currentParentValue = this.parentElement.find('select').val();
			if(!isNaN(currentParentValue))
				this.initPlugin(currentParentValue);
		} else {
			this.initPlugin(null);
		}


        if (typeof this.value !== 'undefined') {
            this.element.val(that.value).trigger('change');
        }

		this.element.on('change', function () {
			that.valueChanged.emit({
				value: that.selector.nativeElement.value
			});
        });
    }

    ngOnDestroy() {
        this.element.off("select2:select");
    }

    private initPlugin(parentValue) {
		let body = {};
		if(this.parentId && (typeof parentValue == 'undefined' || parentValue == null)) {
			return;
		}
		if(typeof parentValue != 'undefined') {
			body = {"id": parentValue};
		}
		if(parentValue != 0){
			let token = this.localStorageService.get('token');
			let headers = new Headers();
			headers.append('Authorization', `Bearer ${token}`);
			headers.append('Content-Type','application/x-www-form-urlencoded');
			return this.http.post(this.linkData, body, {headers:headers}).subscribe(
				(response: Response) =>  {
					let data = response.json();
					var flag = false;

					data.forEach((val)=>{
						if(this.selected == val.id){
							flag = true;
						}
					});
					if(!flag) this.selected = '0';
					// If select2 already initialized remove him and remove all tags inside
					if (this.element.hasClass('select2-hidden-accessible') == true) {
						this.element.select2('destroy');
						this.element.html('');
					}

					if(this.placeholder) {
						data.unshift({id: 0, text: this.placeholder});
					}

					this.element.select2({
						data: data,
						theme: (this.theme) ? this.theme : 'default',
						width: (this.width) ? this.width : '100%',
					});

					if(this.placeholder) {
						this.element.val(0);
					}

					if(this.firstSelectOption == 'true'){
						this.element.val(data[1].id);
					}else {
						this.element.val(0);
					}

					if(parentValue != null && this.selected != 0){
						//this.firstSelectOption = 'false';
						if(data[1]){
							this.element.val(data[1].id);
						}
					}


					if(typeof this.selected != 'undefined' && this.selected != 0){
						this.element.val(this.selected);
					}




					this.element.trigger("change");
				}
			);
		}else {
			let data = [];
			// If select2 already initialized remove him and remove all tags inside
			if (this.element.hasClass('select2-hidden-accessible') == true) {
				this.element.select2('destroy');
				this.element.html('');
			}

			if(this.placeholder) {
				data.unshift({id: 0, text: this.placeholder});
			}

			this.element.select2({
				data: data,
				theme: (this.theme) ? this.theme : 'default',
				width: (this.width) ? this.width : '100%',
			});

			if(this.placeholder) {
				this.element.val(0);
			}

			if(this.firstSelectOption == 'true'){
				this.element.val(data[1].id);
			}else {
				this.element.val(0);
			}


			if(parentValue != null){
				//this.firstSelectOption = 'false';
				if(data[1]){
					this.element.val(data[1].id);
				}
			}

			if(typeof this.selected != 'undefined'){
				//this.element.val(this.selected);
			}

			this.element.trigger("change");
		}

    }
}