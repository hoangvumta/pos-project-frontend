import {Component, Input} from '@angular/core';
import { ValidationService } from './validation.service';

@Component({
    selector: 'control-messages', // attribute property cua component ControlMessages
    template: `<div class="f-error" style="margin-top: 10px; color: red" *ngIf="errorMessage !== null">{{errorMessage}}</div>`,
    styles: [`
        .alert {
            padding: 10px !important;
        }
    `]
})
export class ControlMessages {
    @Input() errors: any;
    @Input() touched: any;
    errorMessage: string;

    constructor() {}

    ngOnInit() {
        this.errorMessage = null;
    }
    ngOnChanges(changes: any){
        if(this.touched && this.errors != null){
            for (let propertyName in this.errors) {
                if (this.errors.hasOwnProperty(propertyName)) {
                    this.errorMessage =  ValidationService.getValidatorErrorMessage(propertyName,this.errors[propertyName]);
                }
            }
        }else{
            this.errorMessage = null;
        }
    }
}