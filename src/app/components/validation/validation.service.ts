export class ValidationService {
    static getValidatorErrorMessage(code:any, errors: any) {// code la xuat ra loi (required, minlength, maxlength)
        console.log('validation Service');
        let minLength = null, maxLength = null;
        if(errors.requiredLength != undefined && errors.actualLength != undefined) { // Xử lý chiều dài chuỗi nhập vào
            if(errors.requiredLength > errors.actualLength){
                minLength = 'Trường nhập vào phải lớn hơn ' + errors.requiredLength + ' ký tự.';
            }else if(errors.requiredLength < errors.actualLength) {
                maxLength = 'Trường nhập vào phải nhỏ hơn ' + errors.requiredLength + ' ký tự';
            }
        }
        let errorServserMessage = '';
        if(code == 'serverErrors'){
            errorServserMessage = errors;
        }
        let config = {
            'required': 'Trường này không được phép để trống.',
            'minlength': minLength,
            'maxlength': maxLength,
            'invalidCreditCard': 'Phải nhập vào số thẻ tín dụng hợp lệ',
            'invalidEmailAddress': 'Địa chỉ email không hợp lệ',
            'invalidPassword': 'Mật khẩu không hợp lệ. Mật khẩu phải có ít nhất 6 ký tự, và chứa một ký tự số.',
            'invalidPhoneNumber': 'Số điện thoại không đúng định dạng',
            'serverErrors': errorServserMessage
        };

        return config[code];
    }

    static creditCardValidator(control) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }
    
    static phoneNumberValidator(control) {
        control = control.value;
        if(control == null || typeof control == "undefined") return { 'invalidPhoneNumber': true };
        let flag = true;
        let phone = control.toString();
        //Bắt đầu bằng số 0
        if(parseInt(phone.slice(0,1)) != 0){
            flag = false;
        }
        //Đầu số phải là 09 or 01
        if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
            flag=false;
        }
        //Đầu 09 độ dài = 10
        if(phone.slice(0,2) == '09'){
            if(phone.length != 10) {
                flag = false;
            }
        }

        //Đầu 01 độ dài 11
        if(phone.slice(0,2) == '01'){
            if(phone.length != 11) flag = false;
        }

        //Độ dài 10-11
        if (phone.length < 10 || phone.length > 11) flag = false;

        if(flag == false){
            return { 'invalidPhoneNumber': true }
            // this.message = 'Số điện thoại không đúng định dạng';
        }else{
            return null;
        }
    }
}
