export class Product {
	constructor(
		public name: string = '',
		public description: string = '',
		public attributes: any = [],
		public skus: any = [],
		public barcode: string = '',
		public hasVariant: boolean = false,
		public isTrackInventory: boolean = true,
		public quantity: number = 0,
		public canPurchaseWhenOutOfStock: boolean = false,
		public unit: string = '',
		public selling_status: number = 1
	) {}
}