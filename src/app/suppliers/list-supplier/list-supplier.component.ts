import {Component, Injectable, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {NgForm} from "@angular/forms";
import {SupplierService} from "../supplier.service";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {Location} from '@angular/common';
import {UrlHelper} from "../../helpers/UrlHelper";
import {ApiService} from "../../helpers/api.service";

declare var $:any;
@Component({
    selector: '[tables-basic]',
    templateUrl: './list-supplier.template.html',
    styleUrls: ['./list-supplier.style.scss'],
})

@Injectable()
export class ListSupplier implements OnInit {
    constructor(
        private router:Router,
        private supplierService:SupplierService,
        private noti: NotificationsService,
        private route: ActivatedRoute,
        private api: ApiService
    ) {}


    provinces = [];
    ngOnInit():void {
        this.api.getProvince().subscribe(
            (pro)=>{
                pro.unshift({id:0,text:'Tỉnh/Thành Phố'});
                this.provinces = pro;
            }
        )
        this.filterData();
    }

    showLoading = false;
    afterView = false;

    //Config pos-table
    data = [
    ];
    settings = {
        columns: [
                {
                    label: '#',
                    value: (model,index)=>{
                        return index+1;
                    }
                },
                {
                label: 'ID',
                value: (model)=>{
                    return model.id;
                },
                sort: {attribute: 'id'},
                filter: {
                    attribute: 'id',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=>{
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            },
            {
                label: 'Tỉnh',
                value: (model)=>{
                    var text = '';
                    this.provinces.forEach((obj,index)=>{
                        if(obj.id == model.province_id){
                            text = obj.text;
                        }
                    })
                    return text;
                },
                filter: {
                    attribute: 'province_id',
                    type: 'dropdown',
                    data: () => {return this.provinces}
                }
            },
            {
                label: 'Thao tác',
                value: (model)=>{
                    let html = '<a [routerLink]="[\'/nha-cung-cap/'+model.id+'/cap-nhat\']" class="glyphicon glyphicon-pencil edit"></a>';
                    html += '<a class="glyphicon glyphicon-trash delete-supplier" data-id="'+model.id+'"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 5,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event){
        this.filterData();
    }

    filterChanged(event){
        this.filterData();
    }

    filterData(){
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.supplierService.getList(page,itemPerpage,filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        )
    }

    //End config pos-table


    ngOnDestroy() {

    }

    ngAfterViewChecked(){
        if(this.afterView){
            if($('.delete-supplier').length > 0){
                $('.delete-supplier').off('click').click((ele)=>{
                    let id = $(ele.currentTarget).attr('data-id');
                    this.onDelete(id);
                });
                this.afterView = false;
            }else{
                // console.log('csss');
                // $('pos-table .select2-container .select2-selection--single').css('height','35px');
                // $('pos-table .select2-selection__rendered').css('line-height','35px');
                // this.afterView = false;
            }
        }
    }


    //Notification
    optionsNoti = {
        position: ["top", "right"],
        timeOut: 4000
    }

    onCreate() {
        this.router.navigate(['/nha-cung-cap', 'them-moi']);
    }

    onDelete(id) {
        this.showLoading = true;
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();
        this.supplierService.delete(id).subscribe(
            (res:any) => {
                if(res.success == 1){
                    // this.data.forEach((item,i)=>{
                    //     if(item.id==res.id){
                    //         this.data.splice(i,1);
                    //     }
                    // })
                    // console.log(this.data);
                    this.noti.success(
                        'Thành Công!',
                        'Xóa thành công'
                    );
                    this.filterData();
                }
            },
            error => {
                console.log('Error')
            },
            ()=> this.showLoading = false
        )
    }
}