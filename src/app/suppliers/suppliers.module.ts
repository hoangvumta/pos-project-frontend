import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//import {DepDrop} from "./../components/depdrop/depdrop.component.ts";
import {FormSupplier} from "./form-supplier/form-supplier.component";
import {ListSupplier} from "./list-supplier/list-supplier.component";
import {ValidationService} from "../components/validation/validation.service";
import {SupplierService} from "./supplier.service";
import {SimpleNotificationsModule} from "angular2-notifications/src/simple-notifications.module";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import { PopoverModule } from 'ng2-popover';

import {PostTable} from "../components/pos-table/pos-table.component";
import {HtmlOutlet} from "../helpers/html-outlet";
import {ApiService} from "../helpers/api.service";

import { PosSharedModule } from "../components/PosSharedModule";

export const routes = [
    {path: '', component:ListSupplier},
    {path: 'them-moi', component: FormSupplier},
    {path: ':id/cap-nhat', component: FormSupplier},
];

@NgModule({
    declarations: [
        FormSupplier,
        ListSupplier,
        //DepDrop,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SimpleNotificationsModule,
        RouterModule.forChild(routes),
        PopoverModule,
		PosSharedModule
    ],
    providers: [ValidationService, SupplierService,NotificationsService,ApiService]
})
export default class SuppliersModule {
    static routes = routes;
}
