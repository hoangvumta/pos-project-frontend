import {Component, OnChanges,Input } from '@angular/core';

@Component({
    selector: 'change',
    template: `
		<h2>Child component</h2>
		{{ src }}
	`
})
class ChangeComponent implements OnChanges {
    @Input() src: string;

    ngOnChanges() {
        console.log('aaa');
    }

}