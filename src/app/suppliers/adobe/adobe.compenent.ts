import {Component, OnInit,OnChanges,Input} from '@angular/core';
import 'rxjs/Rx';
import {ApiService} from "../../helpers/api.service";

declare var Aviary: any;
@Component({
    selector: '[adobe]',
    template: `
<div class="row">
    <div class="col-md-9">
        <img id="minimon" (click)="edit(image)" class="img-responsive" src="http://topanhdep.net/wp-content/uploads/2016/03/hinh-anh-minion-3.jpg" name="minimon" #image/>    
    </div>
    <div class="col-md-9">
        <img id="pikachu" (click)="edit(image2)" class="img-responsive" src="http://topanhdep.net/wp-content/uploads/2015/09/hinh-anh-pokemon-9.jpg" name="pikachu" #image2/>    
    </div>
    `,
    styleUrls: ['./adobe.style.scss'],
})

export class Adobe implements OnInit{
    image : any
    constructor(private apiService:ApiService) {
        var currentImage; // assigned when the Edit button is clicked
        // Image Editor configuration
        var csdkImageEditor = new Aviary.Feather({
            apiKey: '4f88d174c2174f788b5464e8f2acd5b7',
            onSave: function(imageID, newURL) {
                currentImage.src = newURL;
                csdkImageEditor.close();
                console.log(newURL);
            },
            onError: function(errorObj) {
                console.log(errorObj.code);
                console.log(errorObj.message);
                console.log(errorObj.args);
            }
        });

    }
    edit(image:string){
        var originalImageSrc = image.src
            console.log(originalImageSrc)
        var currentImage; // assigned when the Edit button is clicked

        let _this = this; // call this of adobe

        // Image Editor configuration
        var csdkImageEditor = new Aviary.Feather({

            apiKey: '4f88d174c2174f788b5464e8f2acd5b7',
            onSave: function(imageID, newURL) {
                currentImage.src = newURL;
                csdkImageEditor.close();
                _this.apiService.saveImage(newURL,imageID).subscribe(
                    res=>{
                        console.log("success")
                    },
                    error=>{
                        console.log(error)
                    }
                )
                console.log(imageID);
            },
            onError: function(errorObj) {
                console.log(errorObj.code);
                console.log(errorObj.message);
                console.log(errorObj.args);
            }
        });

        var id = image.id
        currentImage = $('#'+id)[0];
        // console.log($('#editable-image2')[0])

        csdkImageEditor.launch({
            image: currentImage.id,
            url: currentImage.src,

        });

    }

    ngOnInit(){

    }

}
