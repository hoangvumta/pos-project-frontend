import {Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/Rx';
import {Router, ActivatedRoute} from "@angular/router";
import {NgForm, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {ApiService} from "../../helpers/api.service";
import {Supplier} from "../supplier";
import {Subscription} from "rxjs/Rx";
import {ValidationService} from "../../components/validation/validation.service";
import {SupplierService} from "../supplier.service";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {Location} from '@angular/common';

declare var $:any;
@Component({
    selector: '[supplier]',
    templateUrl: './supplier.template.html',
    styleUrls: ['./supplier.style.scss'],
	encapsulation: ViewEncapsulation.None
})
export class FormSupplier implements OnInit{
    supplierId = null;
    private subscription: Subscription;
    //Khai bao model Supplier
    supplier: Supplier;
    supplierForm: FormGroup;

    //Loading button
    loading = false;

    //Khai bao deepdrop
    selected: any;
    idProvince =0;
    idDistrict = 0;
    idTown =0;


    //Notification
    optionsNoti = {
        position: ["top", "right"],
        timeOut: 4000
    }

    constructor(
        private http: Http,
        private apiService : ApiService,
        private router:Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private supplierService: SupplierService,
        private noti:NotificationsService,
        private location:Location
    ){}

    ngOnInit() {
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if(params.hasOwnProperty('id')) {
                    this.supplierId = params.id;
                }else {
                    this.supplierId = null;
                }
                this.initForm();
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    initForm() {
        //Form Products
        if(this.supplierId != null){
            this.supplierService.getSupplierDetail(this.supplierId).subscribe(
                (data:any)=>{
                    this.supplierForm = this.formBuilder.group({
                        name: [data.name, Validators.required],
                        province_id: [data.province_id,Validators.required],
                        district_id: [data.district_id,Validators.required],
                        town_id: [data.town_id,Validators.required],
                        address: [data.address,Validators.required],
                        phone: [data.phone,[
                            Validators.required,
                            ValidationService.phoneNumberValidator
                        ]]
                    });
                    this.idProvince = data.province_id;
                    this.idDistrict = data.district_id;
                    this.idTown = data.town_id;

                },
                (error)=>console.log(error)
            )

        }
        else{
            this.supplierForm = this.formBuilder.group({
                name: ['', Validators.required],
                province_id: [this.idProvince,Validators.required],
                district_id: [this.idDistrict,Validators.required],
                town_id: [this.idTown,Validators.required],
                address: ['',Validators.required],
                phone: ['',[
                    Validators.required,
                    ValidationService.phoneNumberValidator
                ]]
            });
        }
    }

    selectProvince($event){
        if($event.value==0) {
            this.supplierForm.controls['province_id'].setValue(null);
        }else {
            this.supplierForm.controls['province_id'].setValue($event.value);
        }
    }
    selectDistrict($event){
        if($event.value==0) {
            this.supplierForm.controls['district_id'].setValue(null);
        }else {
            this.supplierForm.controls['district_id'].setValue($event.value);
        }
    }
    selectTown($event){
        if($event.value==0) {
            this.supplierForm.controls['town_id'].setValue(null);
        }else {
            this.supplierForm.controls['town_id'].setValue($event.value);
        }
    }

    onSubmit(){
        if(this.supplierId){
            this.loading = true;
            this.supplierService.edit(this.supplierForm.value,this.supplierId).subscribe(
                (data:any) =>{
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Lưu thành công',
                        );
                    }else{
                        for (let propertyName in data.errors) {
                            this.supplierForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.loading = false
            )
        }else{
            this.loading = true;
            this.supplierService.create(this.supplierForm.value).subscribe(
                (data:any) =>{
                    console.log(data);
                    if(data.success == 1){
                        this.noti.success(
                            'Thành Công!',
                            'Đã thêm nhà cung cấp mới',
                        );
                        this.supplierId = data.id;
                        this.location.replaceState("/nha-cung-cap/"+this.supplierId+"/cap-nhat");
                    }else{
                        for (let propertyName in data.errors) {
                            this.supplierForm.controls[propertyName].setErrors({serverErrors: data.errors[propertyName][0]});
                        }
                    }
                },
                err => console.log(err),
                () => this.loading = false
            )
        }
    }
    //End

}