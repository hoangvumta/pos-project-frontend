import { Injectable } from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import {Supplier} from "./supplier";
import {Observable} from "rxjs/Rx";
import {stringify} from "@angular/core/src/facade/lang";
import {AppConfig} from '../app.config';
@Injectable()

export class SupplierService{
    constructor(private http: Http, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    public create(supplier: Supplier): Observable<boolean> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/supplier/create','data='+JSON.stringify(supplier),{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public edit(supplier: Supplier, id): Observable<boolean> {
        supplier.id = id;
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/supplier/edit','data='+JSON.stringify(supplier),{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public delete(id): Observable<boolean> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.baseApiUrl+'/supplier/delete','data='+id,{
            headers: headers
        }).map((response: Response) => response.json());
    }

    public getSupplierDetail(id){
        return this.http.get(this.config.baseApiUrl+'/supplier/supplier-detail?id='+id)
            .map((res) => res.json());
    }

    public getList(page?,itemPerPage?,filters?){
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        return this.http.get(this.config.baseApiUrl+'/supplier/get-list-supplier?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters))
            .map((res) => res.json());
    }
}