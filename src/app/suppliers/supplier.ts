export class Supplier {
  constructor(
      public id: number,
      public name: string,
      public province_id: number,
      public district_id: number,
      public town_id: number,
      public address: string,
      public created_at: string,
      public phone: string
  ) {}
}
