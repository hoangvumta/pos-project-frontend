import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';
@Injectable()

export class MjProductService{
    constructor(private http: Http, private localStorageService: LocalStorage, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    getProduct(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/product/get-product-moji?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }
    
    addTag(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/tag/create',body,{headers:headers})
            .map(res=>res.json())
    }
    
    getTag(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/tag/get-tag', {headers:headers}) 
            .map(res => res.json())
    }
    
    deleteTag(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/tag/delete?id='+id,{headers:headers})
            .map(res=>res.json())
    }
    

}