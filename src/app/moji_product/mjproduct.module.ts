import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {MjProduct} from "./list/list.component";
import {MjProductService} from "./mjproduct.service";
import { PosSharedModule } from "../components/PosSharedModule";
export const routes = [
    {path: '', component:MjProduct},
];

@NgModule({
    declarations: [
        MjProduct

    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
		PosSharedModule
    ],
    providers: [MjProductService]
})
export default class MjProductModule {
    static routes = routes;
}
