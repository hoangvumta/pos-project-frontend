import {Component, Injectable, OnInit, Renderer, ElementRef, OnDestroy, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {UrlHelper} from "../../helpers/UrlHelper";
import {MjProductService} from "../../moji_product/mjproduct.service";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import { Tags} from "../tags";
import {NgForm} from "@angular/forms";
import {timeout} from "rxjs/operator/timeout";
declare var $:any;
@Component({
  selector: '[tables-basic]',
  templateUrl: './list.template.html',
  styleUrls: ['./list.style.scss']
})

@Injectable()
export class MjProduct implements OnInit{
    listenFunc: Function;
    globalListenFunc: Function;
    complexForm : FormGroup;
    model = new Tags();
    tags = [];
  constructor(private mjproductService:MjProductService,private elementRef: ElementRef,private renderer: Renderer)
  {

  }
    
    ngOnInit():void {
		console.log('list component init');
        this.filterData();
    }

    showLoading = false;
    afterView = false;
    
    

    filterTag(){

    }

    //Config pos-table
    data = [];
    settings = {
        columns: [
            {
                label: '#',
                value: (model,index)=>{
                    return index+1;
                }
            },
            {
                label: 'ID Nhanh',
                value: (model)=>{
                    return model.id_nhanh;
                },
                sort: {attribute: 'id_nhanh'},
                filter: {
                    attribute: 'id_nhanh',
                    type: 'input'
                }
            },
            {
                label: 'Mã vạch',
                value: (model)=>{
                    return model.sku;
                },
                sort: {attribute: 'sku'},
                filter: {
                    attribute: 'sku',
                    type: 'input'
                }
            },
            {
                label: 'Tên',
                value: (model)=>{
                    return model.name;
                },
                sort: {attribute: 'name'},
                filter: {
                    attribute: 'name',
                    type: 'input'
                }
            },
            {
                label: 'Tags',
                value: (model)=>{
                    let id = model.id_nhanh;
                    let html = '<div id="show-tag-'+id+'">';
                    if(this.tags[id]){
                        this.tags[id].forEach((obj, index)=>{
                            html += '<div class="tag-input" style="border: 1px solid black;border-radius:3px;position: relative; padding: 3px 6px;float: left;margin-right: 5px;margin-top: 5px;">' +
                                        '<span class="tag-label" style="color: green;font-family: sans-serif;">'+obj.tag+'</span>' +
                                        '<span class="tag-remove" data_id="'+obj.id+'" style="color: black;cursor: pointer;padding-left: 10px;">×</span>' +
                                    '</div>';
                        });
                    }
                    html += '</div>'
                    return html;
                },
                filter: {
                    attribute: 'tag',
                    type: 'input'
                },
                format: 'raw'
            },
            {
                label: '',
                value: (model)=>{
                    let html = '<a class="glyphicon glyphicon-plus tags-' + model.id_nhanh + '" id="' +  model.id_nhanh + '"></a>';
                    return html;
                },
                format: 'raw'
            },
        ],
        pagination: {
            currentPage: 1,
            maxSize: 10,
            itemsPerPage: 10,
            totalItems: 0,
        }
    }

    pageChanged(event:any):void {
        this.filterData();
    };

    sortChanged(event){
        this.filterData();
    }

    filterChanged(event){
        this.filterData();
    }

    filterData(){
		console.log('build data table');
        //get tags
        this.mjproductService.getTag().subscribe(
            res => {
                this.tags = res;
            }
        )
        //Phân trang
        let page = UrlHelper.getParams()['page'];
        let itemPerpage = this.settings.pagination.itemsPerPage;

        //End phân trang

        //param sort, filter
        let filters = UrlHelper.getParams();

        this.showLoading = true;
        this.mjproductService.getProduct(page,itemPerpage,filters).subscribe(
            res => {
                this.data = res.data;
                this.settings.pagination.totalItems = res.totalItems;
                this.settings.pagination.currentPage = page;
                this.afterView = true;
            },
            error => {
                console.log('Error');
            },
            ()=>this.showLoading = false
        )
        this.listenFunc = this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {
            let id = event.srcElement.id;
            $('.tags-'+id).popover({
                placement: 'left',
                container: 'body',
                html: true,
                title: 'Thêm tag',
                content: function () {
                    var html =
                        '<input type="text" class="form-control" name="tag" #tag> ' +
                        '<a class="btn btn-success smtag" style="margin-top: 10px;">Thêm tag </a>'
                    return html;
                }
            });
            $('.tags-'+id).popover('show');

            let that = this;
            $('.smtag').closest('.popover-content').off('click').on('click', function (e){
                let text = $(this).find('input[name="tag"]').val();
                that.model.id_product = id;
                that.model.tag = text;
                if(text && id){
                    that.mjproductService.addTag(that.model).subscribe(
                        res => {
                            let html = '<div class="tag-input" style="border: 1px solid black;border-radius:3px;position: relative; padding: 3px 6px;float: left;margin-right: 5px;margin-top: 5px;">' +
                                '<span class="tag-label" style="color: green;font-family: sans-serif;">'+text+'</span>' +
                                '<span class="tag-remove" data_id="'+res.id+'" style="color: black;cursor: pointer;padding-left: 10px;">×</span>' +
                                '</div>';
                            $('#show-tag-'+id).append(html);
                            $('.tags-'+id).popover('hide');
                        },
                        err => console.log('err')
                    )
                }
            });
            $('.tag-remove').off('click').on('click', function () {
                let id = $(this).attr('data_id');
                that.mjproductService.deleteTag(id).subscribe(
                    res => {
                        $(this).parent().remove();
                    },
                    err => console.log('err')
                )
            })


        });
    }

    //End config pos-table


    ngOnDestroy() {
        //this.listenFunc();
        //this.globalListenFunc();
    }

    ngAfterViewChecked() {
    }

    
}