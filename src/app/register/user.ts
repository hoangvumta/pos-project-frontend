export class User {
    constructor(
        public username: string = null,
        public password: string = null,
        public email: string = null,
        public phone: string = null,
        public code: string = null
    ) {  }
}
