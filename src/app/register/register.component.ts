import {Component, ViewEncapsulation, OnInit, Injectable} from '@angular/core';
import {NgForm} from "@angular/forms";

import {Headers, Http} from "@angular/http";
import {Router} from "@angular/router";
import {User} from "./user";
import {ApiService} from "../helpers/api.service";



@Component({
    selector: 'register',
    styleUrls: [ './register.style.scss' ],
    templateUrl: './register.template.html',
    encapsulation: ViewEncapsulation.None,
    host: {
        class: 'register-page app'
    }
})
@Injectable()
export class Register implements OnInit{
   constructor(private http:Http,private router:Router, private apiService : ApiService){}
   ngOnInit(){
       localStorage.setItem("warning","");
       localStorage.setItem("phone-mess","");
   }
    user = new User();
    message = '';
    mess = '';

    getcode(phone : string){
        if(phone.length<10){
            localStorage.setItem("phone-mess","Số điện thoại chưa đúng định dạng !");
            location.reload();
        }
        let phoneNumber = phone;
        this.message = null;
        this.apiService.getCode(phoneNumber).subscribe(
            res =>{
                console.log('success !');
                this.mess = ' Gửi mã xác nhận thành công !'
            },
            error => {
                this.mess = 'Có lỗi xảy ra';
            }
        )

    }
    onSubmit(form: NgForm){

       this.apiService.register(this.user.email,this.user.password,this.user.username,this.user.phone,this.user.code).subscribe(
           user => {
               console.log('success');
               localStorage.setItem("register","Đăng ký thành công !");
               this.router.navigateByUrl('login');
           },
           errors => {
               this.message = 'Đã có lỗi xảy ra ! Vui lòng thử lại !';
           }
       )
    }

}
