import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {Http} from "@angular/http";
declare var $: any;
declare var jQuery:any;
@Component({
    selector: 'forecast',
    styleUrls: [ './forecast.style.scss' ],
    templateUrl: './forecast.template.html',
    encapsulation: ViewEncapsulation.None,
    host: {
        class: 'forecast-page app'
    }
})

export class Forecast implements OnInit{

    constructor(private http: Http) {
        
    }
    ngOnInit(){
        
    }
     flag = false;

    Onchange($event, params){
        if (params == 'mat-hang') {
            if ($event.target.value == 'nhap-moi') {
                $('#code-product').hide();
            } else {
                $('#code-product').show();
                $('#ty-suat').hide();
            }
        }

        if (params == 'danh-muc'){
            if($event.target.value == 'category-new'){
                $('#ty-suat').show();
            }else{
                $('#ty-suat').hide();
            }
        }
     }

}
