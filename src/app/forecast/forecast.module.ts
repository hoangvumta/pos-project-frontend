import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Forecast } from './forecast.component';

export const routes = [
    { path: '', component: Forecast, pathMatch: 'full' }
];

@NgModule({
    declarations: [
        Forecast
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
    ]
})
export default class ForecastModule {
    static routes = routes;
}
