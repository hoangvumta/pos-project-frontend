import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {Company} from "./create/company.component.ts";
import {ListCompany} from "./list-company/list-company.component.ts";
import {DepDrop} from "./../components/depdrop/depdrop.component.ts";
import {EditCompany} from "./edit-company/edit-company.component";
import {CompanyService} from "./companies.service";
import {PostTable} from "../components/pos-table/pos-table.component";
import {HtmlOutlet} from "../helpers/html-outlet";
import {PaginationModule} from "ng2-bootstrap/ng2-bootstrap";
export const routes = [
    {path: '', redirectTo: 'create', pathMatch: 'full'},
    {path: 'them-moi', component: Company},
    {path: '', component:ListCompany},
    {path: ':id/cap-nhat', component: EditCompany},
];

@NgModule({
    declarations: [
        Company,
        ListCompany,
        DepDrop,
        EditCompany,
        PostTable,
        HtmlOutlet

    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
        PaginationModule
    ],
    providers: [CompanyService]
})
export default class CompaniesModule {
    static routes = routes;
}
