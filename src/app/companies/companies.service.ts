import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';
@Injectable()

export class CompanyService{
    constructor(private http: Http, private localStorageService: LocalStorage , config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    getCompany(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/company/get-list-company?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }

    createCompany(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/company/create',body,{headers:headers})
            .map(res=>res.json())
    }

    editCompany(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/company/update',body,{headers:headers})
            .map(res=>res.json())
    }

    deleteCompany(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/company/delete?id='+id,{headers:headers})
            .map(res=>res.json())
    }

    detailCompany(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/company/view?id='+id,{headers:headers})
            .map(res=>res.json())
    }

}