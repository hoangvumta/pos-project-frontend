import {Component, Injectable, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { Companies} from "../companies";
import {ApiService} from "../../helpers/api.service";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {CompanyService} from "../companies.service";


@Component({
    selector: 'edit',
    templateUrl: './edit-company.template.html',
    styleUrls: ['./edit-company.style.scss'],
})

@Injectable()

export class EditCompany implements OnInit{
    selected: any;
    dataprovince : any[];
    datadistrict : any[];
    dataward : any[];
    selectedProvince: any;
    selectedDistrict: any;
    selectedWard: any;

    complexForm : FormGroup;
    loading = false;
    

    constructor(private apiService:ApiService,private router: Router, private route: ActivatedRoute, private companyService:CompanyService){
        this.complexForm = new FormGroup({
            'name' : new FormControl('',Validators.required ),
            'phone' : new FormControl('',[
                Validators.required,
                validatePhone
            ]),
            'address' : new FormControl('',Validators.required ),
            'notes' : new FormControl(),
            'email' : new FormControl('',[Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
            'id' : new FormControl('',Validators.required ),
        });

        function validatePhone($att:FormControl) {
            $att = $att.value;
            if($att == null || typeof $att == "undefined") return {phoneValid:false};
            let flag = true;
            let phone = $att.toString();
            //Bắt đầu bằng số 0
            if(parseInt(phone.slice(0,1)) != 0){
                flag = false;
            }
            //Đầu số phải là 09 or 01
            if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
                flag=false;
            }
            //Đầu 09 độ dài = 10
            if(phone.slice(0,2) == '09'){
                if(phone.length != 10) {
                    flag = false;
                }
            }

            //Đầu 01 độ dài 11
            if(phone.slice(0,2) == '01'){
                if(phone.length != 11) flag = false;
            }

            //Độ dài 10-11
            if (phone.length < 10 || phone.length > 11) flag = false;

            if(flag == false){

                return {phoneValid:false}
                // this.message = 'Số điện thoại không đúng định dạng';
            }else{
                return null;
            }
        }
    }
    
    message =null;
    warning ='';
    danger ='';
    model = new Companies();
    submitted = false;
    ngOnInit(){
        this.route.params.subscribe(
            (params: any) => {
                // console.log(params.id)
                this.companyService.detailCompany(params.id).subscribe(
                    res =>{
                        // console.log(res.data);
                        this.model.id = res.id;
                        this.model.name = res.name;
                        this.model.phone = res.phone;
                        this.model.address = res.address;
                        this.model.notes = res.notes;
                        this.model.province_id = res.province_id;
                        this.model.district_id = res.district_id;
                        this.model.ward_id = res.ward_id;
                        this.model.email = res.email;
                    },
                    error => {
                        this.router.navigate(['/doanh-nghiep']);
                    }
                )
            }
        );
    }

    onChangeSelectProvince($event){
        this.model.province_id = $event.value;
    }

    onChangeSelectDistrict($event){
        this.model.district_id = $event.value;
        this.apiService.getDistrict(this.model.province_id).subscribe(
            res =>{
                this.datadistrict = res
            },
            error => {
                console.log('Error');
            }
        )
    }

    onChangeSelectWard($event){
        this.model.ward_id = $event.value;
        this.apiService.getTown(this.model.district_id).subscribe(
            res =>{
                this.dataward = res
            },
            error => {
                console.log('Error');
            }
        )
    }

    changePro(){
        this.model.district_id = this.datadistrict[0].value;
        this.model.ward_id = this.dataward[0].value;
    }
    changeDis(){
        this.model.ward_id = this.dataward[0].value;
    }

    editCompany(complexForm){
        this.loading = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0){
            this.message = 'Vui lòng chọn địa chỉ';
            return false ;
        }
        this.companyService.editCompany(this.model).subscribe(
            res => {
                if(res.success == 1) {
                    this.router.navigate(['/doanh-nghiep']);
                }else{
                    var messages = res.messages;
                    messages.forEach((message) => {
                        this.complexForm.controls[message.property].setErrors({
                            remote: message.message});
                    });
                }

            },
            error => {
                console.log('Error')
            },
            ()=> this.loading = false
        );
    }
}