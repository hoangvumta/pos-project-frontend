import {Component, OnInit } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/Rx';
import { Companies } from '../companies';
import {Router} from "@angular/router";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {CompanyService} from "../companies.service";

@Component({
    selector: '[company]',
    templateUrl: './company.template.html',
    styleUrls: ['./company.style.scss'],
})
export class Company implements OnInit{
	selected: any;

    selectedProvince: any;
    selectedDistrict: any;
    selectedWard: any;

    message =null;
    warning ='';
    danger ='';
    model = new Companies();
    submitted = false;

    complexForm : FormGroup;
    loading = false;
    

    constructor(private http: Http,private companyService:CompanyService,private router: Router){
        this.complexForm = new FormGroup({
            'name' : new FormControl('',Validators.required),
            'phone' : new FormControl('',[
                Validators.required,
                validatePhone
            ]),
            'address' : new FormControl('',Validators.required ),
            'notes' : new FormControl(),
            'email' : new FormControl('',[Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")] ),
        });

        function validatePhone($att:FormControl) {
            $att = $att.value;
            if($att == null || typeof $att == "undefined") return {phoneValid:false};
            let flag = true;
            let phone = $att.toString();
            //Bắt đầu bằng số 0
            if(parseInt(phone.slice(0,1)) != 0){
                flag = false;
            }
            //Đầu số phải là 09 or 01
            if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
                flag=false;
            }
            //Đầu 09 độ dài = 10
            if(phone.slice(0,2) == '09'){
                if(phone.length != 10) {
                    flag = false;
                }
            }

            //Đầu 01 độ dài 11
            if(phone.slice(0,2) == '01'){
                if(phone.length != 11) flag = false;
            }

            //Độ dài 10-11
            if (phone.length < 10 || phone.length > 11) flag = false;

            if(flag == false){

                return {phoneValid:false}
                // this.message = 'Số điện thoại không đúng định dạng';
            }else{
                return null;
            }
        }
    }

    ngOnInit() {
    }


    onChangeSelectProvince($event){
        this.model.province_id = $event.value;
    }

    onChangeSelectDistrict($event){
        this.model.district_id = $event.value;
    }

    onChangeSelectWard($event){
        this.model.ward_id = $event.value;
    }


    submitForm(complexForm){
        this.loading = true;
        this.message = null;
        this.submitted = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0){
            this.message = 'Vui lòng chọn địa chỉ';
            return false ;
        }
        this.companyService.createCompany(this.model).subscribe(
            res => {
                if(res.success == 1) {
                    this.message = 'Thêm mới thành công';
                    this.router.navigate(['/doanh-nghiep']);
                }else{
                    var messages = res.messages;
                    messages.forEach((message) => {
                        this.complexForm.controls[message.property].setErrors({
                            remote: message.message});
                    });
                }
            },
            err => console.log(err),
            () => this.loading = false
        );
    }
}