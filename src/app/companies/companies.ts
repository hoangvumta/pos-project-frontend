export class Companies {
    constructor(
        public id: number =null,
        public name: string = null,
        public notes: string = null,
        public address: string = null,
        public phone: string = null,
        public email: string = null,
        public province_id: number = null,
        public district_id: number = null,
        public ward_id: number = null,
        public status: number = null

    ) {  }
}