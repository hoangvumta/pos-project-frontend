import {Component, OnInit} from '@angular/core';
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {ApiService} from "../../helpers/api.service";

@Component({
  selector: '[tables-basic]',
  styleUrls: ['./customer-list-source.style.scss'],
  template: require('./tables-basic.template.html')
})
export class CustomerList implements OnInit{
  
  datas = [];
  constructor(private http: Http, private router: Router, private apiService: ApiService){
    
  }
  
  ngOnInit() {
    this.apiService.getCustomer().subscribe(
      (res: any) => this.datas = res
  );
  }

  onCreate(){
    this.router.navigate(['/khach-hang', 'them-moi']);
  }
  onChange(form: NgForm){
    console.log(form.value);
  }
}
