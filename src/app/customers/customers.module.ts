import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomerList } from './list/customer-list.component';

import {Customer} from "./create/customer.component.ts";

import {DepDrop} from "./../components/depdrop/depdrop.component.ts";
export const routes = [
    {path: '', redirectTo: 'create', pathMatch: 'full'},
    {path: 'them-moi', component: Customer},
    {path: '', component: CustomerList},
];

@NgModule({
    declarations: [
        Customer,
        CustomerList, DepDrop
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes)
    ]
})
export default class CustomerModule {
    static routes = routes;
}