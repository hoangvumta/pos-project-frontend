import {Component, } from '@angular/core';
declare var jQuery: any;
@Component({
    selector: '[customer]',
    templateUrl: './customer.template.html',
    styleUrls: ['./customer.style.scss']
})
export class Customer{
    selected: any;
    constructor(){}

    ngOnInit(): void {}

    tags = [
        {name: 'tag', value:['Adidas', 'Nike']},
    ];
    removeTags(values, i){
        let index = this.tags[i].value.indexOf(values);
        this.tags[i].value.splice(index,1);
    }
    addTag(event,i){
        if(event.target.value != ''){
            let oldValue = [];
            let values = event.target.value;
            oldValue = this.tags[i].value;

            var flag = true;
            for (var item of oldValue) {
                if(String(item).toLowerCase() == String(values).toLowerCase()){
                    flag = false;
                }
            }

            if(flag) {
                oldValue.push(event.target.value);
                event.target.value = '';
                jQuery(event.target).attr('data-original-title','');
                jQuery(event.target).tooltip("hide");
            }else{
                jQuery(event.target).attr('data-original-title','Đã có');
                jQuery(event.target).tooltip('show');
            }
        }
    }
}