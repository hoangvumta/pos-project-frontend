import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService, DataService, SocketService } from '../helpers/services';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.template.html'
})
export class Dashboard {
	constructor( private apiService: ApiService, private router: Router,
		private socketService: SocketService, private dataService: DataService ) {
	}

	ngOnInit() {
		if (this.dataService.authStatus$.getValue() === true) {
			console.log('authStatus true');
		} else {
			console.log('authStatus false');
			this.dataService.authStatus$.subscribe(status => {
				if (status === true) {
					console.log('setup component');
					//this.setupComponent();
				} else {
					let currentUrl = this.router.url;
					let returnUrl = encodeURIComponent(currentUrl);
					this.router.navigateByUrl(`/login?returnUrl=${returnUrl}`);
				}
			});
		}
	}
}
