import { Component, ViewEncapsulation } from '@angular/core';
import { Credentials } from './credentials';
import { ApiService } from '../helpers/services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'login',
    styleUrls: [ './login.style.scss' ],
    templateUrl: './login.template.html',
    encapsulation: ViewEncapsulation.None,
    host: {
        class: 'login-page app'
    }
})
export class Login {
    constructor( private apiService: ApiService, private route: ActivatedRoute, private router: Router) {}


    model = new Credentials();
    submitted = false;
    message = null;
    loading = false;

    register = localStorage.getItem("register");


    ngOnInit() {
        console.log('On Init Login');
        localStorage.setItem("register","");
    }

    onSubmit() {
        this.message = null;
        this.submitted = true;
        this.loading = true;
        console.log('Submit');
        this.apiService.login(this.model.email, this.model.password).subscribe(
            user => {
                if(user){
                    let returnUrl = this.route.snapshot.queryParams['returnUrl'];
                    if (returnUrl) {
                        let decodedReturnUrl = decodeURIComponent(returnUrl);
                        this.router.navigateByUrl(decodedReturnUrl);
                    } else {
                        this.router.navigateByUrl('/dashboard');
                    }
                }else{
                    this.message = 'Sai tên đăng nhập hoặc mật khẩu';
                }

            },
            error => {
                this.message = 'Tên đăng nhập hoặc mật khẩu không chính xác';
                console.log('failed');
            },
            ()=> this.loading = false
        );
    }
}