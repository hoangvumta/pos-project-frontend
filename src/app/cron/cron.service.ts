import { Injectable } from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';

@Injectable()

export class CronService{
    constructor(private http: Http, private localStorageService: LocalStorage, config: AppConfig){
        this.config = config.getConfig();
    }
    
    config: any;

    getListCronTime(page?,itemPerPage?,filters?) {

        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/mj-cron/list-cron?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }
    getListCronDetail(id,page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/mj-cron/list-cron-detail?id='+id+'&page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }
    
    
}