import {Component, Injectable, OnInit} from "@angular/core";
import {CronService} from "../cron.service";
import {UrlHelper} from "../../helpers/UrlHelper";
import {Subscription} from "rxjs/Rx";
import { ActivatedRoute} from "@angular/router";
declare var $:any;

@Component({
  selector: '[tables-basic]',
  templateUrl: './list.template.html',
  styleUrls: ['./list.style.scss']
})

@Injectable()
export class ListCronDetail implements OnInit {
	private subscription: Subscription;
	cronId = null;
	constructor(
		private cronService: CronService,
		private route: ActivatedRoute
	)
	{}
	

	ngOnInit():void {
		this.subscription = this.route.params.subscribe(
			(params: any) => {
				if(params.hasOwnProperty('id')) {
					this.cronId = params.id;
				}else {
					this.cronId = null;
				}
			}
		);
		this.filterData();
	}

	showLoading = false;
	afterView = false;
	

	//Config pos-table
	data = [
	];
	settings = {
		columns: [
			{
				label: '#',
				value: (model,index)=>{
					return index+1;
				}
			},
			{
				label: 'Tên sản phẩm',
				value: (model)=>{
					let name = model.product_name.split('-');
					let html = '';
					html = '<p>'+model.product_name+'  <a href="http://www.nhaphang247.com/admin/moji/list-order?product_name='+name[0]+'" target="_blank"><i class="glyphicon glyphicon-link"></i></a></p>';
					return html;
				},
				format: 'raw'
			},
			{
				label: 'Mã sản phẩm',
				value: (model)=>{
					return model.sku;
				}
			},
			{
				label: 'Số lượng cần nhập',
				value: (model)=>{
					return model.need_import_quantity;
				}
			},
			{
				label: 'Bán trong ngày',
				value: (model)=>{
					return model.sold_out_on_day;
				}
			},
			{
				label: 'Thời gian lấy dữ liệu',
				value: (model)=>{
					return model.time_to_take_data;
				}
			},
			{
				label: 'Thời gian để bán hàng',
				value: (model)=>{
					return model.time_for_sale;
				}
			},
			{
				label: 'Đã bán',
				value: (model)=>{
					return model.total_sold_out;
				}
			},
			{
				label: 'Tồn kho',
				value: (model)=>{
					return model.inventory;
				}
			},
			{
				label: 'Thời gian nhập hàng',
				value: (model)=>{
					return model.time_for_transport;
				}
			},
		],
		pagination: {
			currentPage: 1,
			maxSize: 5,
			itemsPerPage: 50,
			totalItems: 0,
		}
	};

	pageChanged(event:any):void {
		this.filterData();
	};

	sortChanged(event){
		this.filterData();
	}

	filterChanged(event){
		this.filterData();
	}

	filterData(){
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;
		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();


		this.showLoading = true;
		this.cronService.getListCronDetail(this.cronId,page,itemPerpage,filters).subscribe(
			res => {
				this.data = res.data;
				this.settings.pagination.totalItems = res.totalItems;
				this.settings.pagination.currentPage = page;
				this.afterView = true;
			},
			error => {
				console.log('Error');
			},
			()=>this.showLoading = false
		)
	}

	//End config pos-table


	ngOnDestroy() {}

	ngAfterViewChecked(){
		if(this.afterView){
			if($('.delete-store').length > 0){
				$('.delete-store').off('click').click((ele)=>{
					let id = $(ele.currentTarget).attr('data-id');
					// this.onDelete(id);
				});
				this.afterView = false;
			}else{
			}
		}
	}


	// onDelete(id) {
	// 	this.showLoading = true;
	// 	//Phân trang
	// 	let page = UrlHelper.getParams()['page'];
	// 	let itemPerpage = this.settings.pagination.itemsPerPage;
    //
	// 	//End phân trang
    //
	// 	//param sort, filter
	// 	let filters = UrlHelper.getParams();
	// 	this.couponService.deleteBill(id).subscribe(
	// 		(res:any) => {
	// 			if(res.success == 1){
	// 				this.filterData();
	// 			}
	// 		},
	// 		error => {
	// 			console.log('Error')
	// 		},
	// 		()=> this.showLoading = false
	// 	)
	// }

}