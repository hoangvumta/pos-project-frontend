import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {ListCronDetail} from "./list-cron-detail/list.component";
import {ListCronTime} from "./list-cron-time/list.component";
import {CronService} from "./cron.service";
import {WareHouseService} from "../warehouse/warehouse.service";
import {SimpleNotificationsModule} from "angular2-notifications/src/simple-notifications.module";
import {NotificationsService} from "angular2-notifications/src/notifications.service";
import {ValidationService} from "../components/validation/validation.service";

import { PosSharedModule } from "../components/PosSharedModule";
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import 'ng2-datetime/src/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js';

export const routes = [
    {path: '', component:ListCronTime},
    {path: ':id/chi-tiet', component:ListCronDetail},
];


@NgModule({
    declarations: [
        ListCronTime,
        ListCronDetail
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
        SimpleNotificationsModule,
		PosSharedModule,
        NKDatetimeModule

    ],
    providers: [CronService,NotificationsService, WareHouseService, ValidationService]
})
export default class CronModule {
    static routes = routes;
}
