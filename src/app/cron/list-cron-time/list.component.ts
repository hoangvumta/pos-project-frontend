import {Component, Injectable, OnInit} from "@angular/core";
import {CronService} from "../cron.service";
import {UrlHelper} from "../../helpers/UrlHelper";
declare var $:any;

@Component({
  selector: '[tables-basic]',
  templateUrl: './list.template.html',
  styleUrls: ['./list.style.scss']
})

@Injectable()
export class ListCronTime implements OnInit {

	constructor(
		private cronService: CronService
	)
	{}

	

	ngOnInit():void {
		this.filterData();
	}

	showLoading = false;
	afterView = false;


	//Config pos-table
	data = [
	];
	settings = {
		columns: [
			{
				label: '#',
				value: (model,index)=>{
					return index+1;
				}
			},
			{
				label: 'Thời gian',
				value: (model)=>{
					let html = '';
					html = '<a [routerLink]="[\'/canh-bao-nhap-hang/'+model.id+'/chi-tiet\']">'+model.start_time+'</a>';
					return html;
				},
				format: 'raw'
			}
		],
		pagination: {
			currentPage: 1,
			maxSize: 5,
			itemsPerPage: 30,
			totalItems: 0,
		}
	}

	pageChanged(event:any):void {
		this.filterData();
	};

	sortChanged(event){
		this.filterData();
	}

	filterChanged(event){
		this.filterData();
	}

	filterData(){
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;
		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();


		this.showLoading = true;
		this.cronService.getListCronTime(page,itemPerpage,filters).subscribe(
			res => {
				this.data = res.data;
				this.settings.pagination.totalItems = res.totalItems;
				this.settings.pagination.currentPage = page;
				this.afterView = true;
			},
			error => {
				console.log('Error');
			},
			()=>this.showLoading = false
		)
	}

	//End config pos-table


	ngOnDestroy() {}

	ngAfterViewChecked(){
		if(this.afterView){
			if($('.delete-store').length > 0){
				$('.delete-store').off('click').click((ele)=>{
					let id = $(ele.currentTarget).attr('data-id');
					// this.onDelete(id);
				});
				this.afterView = false;
			}else{
			}
		}
	}


	// onDelete(id) {
	// 	this.showLoading = true;
	// 	//Phân trang
	// 	let page = UrlHelper.getParams()['page'];
	// 	let itemPerpage = this.settings.pagination.itemsPerPage;
    //
	// 	//End phân trang
    //
	// 	//param sort, filter
	// 	let filters = UrlHelper.getParams();
	// 	this.couponService.deleteBill(id).subscribe(
	// 		(res:any) => {
	// 			if(res.success == 1){
	// 				this.filterData();
	// 			}
	// 		},
	// 		error => {
	// 			console.log('Error')
	// 		},
	// 		()=> this.showLoading = false
	// 	)
	// }

}