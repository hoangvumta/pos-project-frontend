import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';
import { SocketService } from './socket.service';
import { DataService } from './data.service';
import { ApiService } from './api.service';
import { LocalStorage } from './localstorage.service';
import {AppConfig} from '../app.config';
// import {Articles} from "../models/articles";


@Injectable()
export class ProductService {
	name: string = 'ProductService';
	token: string = null;
	user: User;
	urlService = '';
  
	constructor(private http: Http, private dataService: DataService, private apiService: ApiService, private localStorageService: LocalStorage, config: AppConfig){
		this.config = config.getConfig();
	}

	config: any;
    getInitDetail(){
		let body = {};
		return this.apiService.postJson(this.config.baseApiUrl+'/product/get-init-detail', body);
	}
	
	getProductDetail(id){
        return this.apiService.getJson(this.config.baseApiUrl+'/product/product-detail?id='+id)
            .map((res) => res.json());
    }
}

