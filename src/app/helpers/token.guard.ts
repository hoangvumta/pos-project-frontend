import { Injectable } from '@angular/core';
import { CanActivate,
         ActivatedRoute,
         Router,
         ActivatedRouteSnapshot,
         RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ApiService, LocalStorage } from './services';

@Injectable()
export class TokenGuard implements CanActivate {

  constructor(private router: Router, private apiService: ApiService,
    private localStorageService: LocalStorage, private route: ActivatedRoute) {}

  canActivate(next:  ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let token = this.localStorageService.get('token');
	let returnUrl = null;
	if(next.queryParams['returnUrl']) {
		returnUrl = next.queryParams['returnUrl'];
	}
    if (token) {
      return this.apiService.loginWithToken(token).map(
        user => this.onSuccessFullLogin(returnUrl),
        err => true
      );
    } else {
      return true;
    }
  }

  onSuccessFullLogin(returnUrl): boolean {
    if (returnUrl) {
      this.router.navigateByUrl(returnUrl);
    } else {
      this.router.navigateByUrl('/dashboard');
    }
    return false;
  }

}
