import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user';
import {SocketService} from './socket.service';
import {DataService} from './data.service';
import {LocalStorage} from './localstorage.service';
import {AppConfig} from '../app.config';


@Injectable()
export class ApiService {
    name:string = 'ApiService';
    token:string = null;
    user:User;

    constructor(private http:Http, private socketService:SocketService,
                private dataService:DataService, private localStorageService:LocalStorage, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    public loginWithToken(aToken:string):Observable<User> {
        this.token = aToken;
        return this.dereferenceToken().catch(err => {
            this.token = null;
            this.localStorageService.remove('token');
            return Observable.throw(err);
        });
    }

    public login(email, password):Observable<User> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {email, password};
        return this.postWithHeaders(this.config.baseApiUrl+'/utility/login', body, headers).map(res => {
            if(res.status == 200 && res.token != null){
                this.token = res.token;
                this.localStorageService.set('token', this.token);
                this.dataService.setAuthStatus(true);
                return res;
            }else{
                this.dataService.setAuthStatus(false);
                return false;
            }
        })
        .catch(err => {
            return Observable.throw(err);
        })
        //.flatMap(() => this.dereferenceToken());
    }

    dereferenceToken():Observable<User> {
        return this.getUserImpl().map(user => {
            this.dataService.setAuthStatus(true);
            return user;
        });
    }

    public logout():void {
        this.token = null;
        this.dataService.setAuthStatus(false);
        this.localStorageService.remove('token');
    }

    public isLoggedIn():boolean {
        return this.token !== null;
    }

    public getUser():User {
        return this.user;
    }

    getUserImpl():Observable<User> {
        let api = this.getUserRest();
        return api.map(
            result => {
                this.user = new User(result.id, result.name, result.email);
                return this.user;
            }
        );
    }

    getUserRest():Observable<any> {
        return this.getJson(this.config.baseApiUrl+'/user/who-am-i');
    }

    postJson(url, body):Observable<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer ${this.token}`);
        return this.postWithHeaders(url, body, headers);
    }

    /* Response looks like this
     _body: "{"error":"Unauthorized"}"
     headers: Headers
     ok: false
     status: 401
     statusText: "Ok"
     type:2
     url: "http://localhost:4000/api/token"
     */
    postWithHeaders(url, body, headers):Observable<any> {
        let post_body = JSON.stringify(body);
        return this.http
            .post(url, post_body, {headers})
            .map(res => res.json())
            .catch((res:Response) => {
                if (res.status === 401) {
                    this.handleUnauthorized();
                }
                return Observable.throw(res);
            });
    }

    getJson(url):Observable<any> {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer ${this.token}`);
        return this.getWithHeaders(url, headers);
    }

    getWithHeaders(url, headers):Observable<any> {
        return this.http
            .get(url, {headers})
            .map(res => res.json())
            .catch((res:Response) => {
                if (res.status === 401) {
                    this.handleUnauthorized();
                }
                return Observable.throw(res);
            });
    }

    handleUnauthorized() {
        this.dataService.setAuthStatus(false);
    }

    getCode(phone) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {phone: phone};
        return this.http.post('http://115.146.127.110:4000/api/send/sms', body, headers).map(res => {
            console.log(res);
        })
    }

    register(email, password, name, phone, code) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {email: email, password: password, name: name, phone: phone, code: code};
        return this.http.post('http://115.146.127.110:4000/api/v1/register', body, headers).map(res => {
            console.log(res);
        })
    }

    // TODO : API Category


    getProvince() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = {id: 1};
        return this.http.post(this.config.baseApiUrl+'/utility/get-province', body, {headers: headers}).map(res => res.json())
    }

    getDistrict(parent_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = {id: parent_id};
        return this.http.post(this.config.baseApiUrl+'/utility/get-district', body, {headers: headers}).map(res => res.json())
    }

    getTown(parent_id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = {id: parent_id};
        return this.http.post(this.config.baseApiUrl+'/utility/get-ward', body, {headers: headers}).map(res => res.json())
    }


    public saveImage(image, name) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = {image: image, name: name};
        return this.http.post('http://localhost/test/save.php', body, headers).map(res=>res)
    }


    /*----------- api them, sua, xoa khách hàng -----------*/

    getCustomer() {
        return this.http.get(this.config.baseApiUrl+'/client/list-client').map(res => res.json()
        )
    }


    uploadFile(fileList){
        let urlUpload = this.config.baseApiUrl+'/upload/upload-file';
        //let urlUpload = 'http://localhost/pos-backend-customer/web/v1/upload/upload-file';
        let formData:FormData = new FormData();
        for (let file of fileList){
            formData.append('uploadFile[]',file);
        }
        let headers = new Headers();
        formData.append('Content-Type', 'multipart/form-data');
        formData.append('Content-Type', 'application/x-www-form-urlencoded');
        formData.append('Accept', 'application/json');
        return this.http.post(urlUpload, formData, true).map(res => res.json());
    }
    
    getInfoUser(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/user/who-am-i', {headers:headers}).map(res => res.json());
    }
    
}

