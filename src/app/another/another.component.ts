import { Component } from '@angular/core';

@Component({
  selector: 'another',
  templateUrl: './another.template.html'
})
export class AnotherPage {
	ngOnInit() {
		console.log('On Init Another');
	}
}
