import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PopoverModule } from 'ng2-popover';
import {WareHouse} from "./create/warehouse.component.ts";
import {ListWareHouse} from "./list/list.component.ts";
//import {DepDrop} from "./../components/depdrop/depdrop.component.ts";
import {EditWareHouse} from "./edit/edit.component";
import {ValidationService} from "../components/validation/validation.service";
import {WareHouseService} from "./warehouse.service";
import {PostTable} from "../components/pos-table/pos-table.component";
import {HtmlOutlet} from "../helpers/html-outlet";
import {PaginationModule} from "ng2-bootstrap/ng2-bootstrap";

import { PosSharedModule } from "../components/PosSharedModule";

export const routes = [
    {path: '', component:ListWareHouse},
    {path: 'them-moi', component: WareHouse},
    {path: ':id/cap-nhat', component: EditWareHouse},
];

@NgModule({
    declarations: [
        WareHouse,
        ListWareHouse,
        //DepDrop,
        EditWareHouse,
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        PopoverModule,
        ReactiveFormsModule,
		PosSharedModule
    ],
    providers: [ValidationService, WareHouseService]
})
export default class WareHouseModule {
    static routes = routes;
}
