import {Component, OnInit } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/Rx';
import { WareHouses} from "../warehouse";
import {WareHouseService} from "../warehouse.service";
import {Router} from "@angular/router";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import 'select2';
declare var $:any;
@Component({
    selector: '[warehouse]',
    templateUrl: './warehouse.template.html',
    styleUrls: ['./warehouse.style.scss']
})
export class WareHouse implements OnInit{
	selected: any;

    selectedProvince: any;
    selectedDistrict: any;
    selectedWard: any;

    message =null;
    warning ='';
    danger ='';
    model = new WareHouses();
    submitted = false;
    public exampleCompany = [];

    complexForm : FormGroup;
    loading = false;

    constructor(private http: Http,private router: Router, private warehouseService: WareHouseService){
        this.complexForm = new FormGroup({
            'name' : new FormControl('',Validators.required ),
            'phone' : new FormControl('',[
                Validators.required,
                validatePhone
            ]),
            'address' : new FormControl('',Validators.required ),
            'description' : new FormControl(),
            'type' : new FormControl('Chọn loại kho',Validators.required),
        });

        function validatePhone($att:FormControl) {
            $att = $att.value;
            if($att == null || typeof $att == "undefined") return {phoneValid:false};
            let flag = true;
            let phone = $att.toString();
            //Bắt đầu bằng số 0
            if(parseInt(phone.slice(0,1)) != 0){
                flag = false;
            }
            //Đầu số phải là 09 or 01
            if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
                flag=false;
            }
            //Đầu 09 độ dài = 10
            if(phone.slice(0,2) == '09'){
                if(phone.length != 10) {
                    flag = false;
                }
            }

            //Đầu 01 độ dài 11
            if(phone.slice(0,2) == '01'){
                if(phone.length != 11) flag = false;
            }

            //Độ dài 10-11
            if (phone.length < 10 || phone.length > 11) flag = false;

            if(flag == false){

                return {phoneValid:false}
                // this.message = 'Số điện thoại không đúng định dạng';
            }else{
                return null;
            }
        }
    }

    ngOnInit() {
        this.warehouseService.getCompany().subscribe(
            data => {
                if(data.length == 1){
                   this.model.company_id = data[0].id;
                }else {
                    data.unshift({id: 0, text: 'Doanh nghiệp'});
                }
                this.exampleCompany = data;
            }
        );
    }


    onChangeSelectProvince($event){
        this.model.province_id = $event.value;
    }

    onChangeSelectDistrict($event){
        this.model.district_id = $event.value;
    }

    onChangeSelectWard($event){
        this.model.ward_id = $event.value;
    }

    onChangeSelectCompany($event){
        this.model.company_id = $event.value;
    }


    submitForm(complexForm){
        this.loading = true;
        this.message = null;
        this.submitted = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0 || this.model.company_id == 0){
            this.message = 'Vui lòng điền đủ thông tin!';
            return false ;
        }

        this.warehouseService.createWarehouse(this.model).subscribe(
            res => {
                this.message = 'Thêm mới kho thành công';
                this.router.navigate(['/kho-hang']);
            },
            err => console.log(err),
            () => this.loading = false
        );
    }

    private afterSelectFilterRender = true;
    ngAfterViewChecked() {
        if($('.filter-select option').length > 0 && this.afterSelectFilterRender){
            $('.filter-select').select2({
                theme: 'bootstrap',
                width: '100%'
            });
            $('.filter-select').change((event)=>{
                this.model.company_id = event.target.value;
                let attr = event.target.attributes.name.value;
                let value = event.target.value;
            })
            this.afterSelectFilterRender =false;
        }
    }

}