import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {AppConfig} from '../app.config';
@Injectable()

export class WareHouseService{
    constructor(private http: Http, private localStorageService: LocalStorage , config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;


    getWarehouse(page?,itemPerPage?,filters?) {
        if(!page) page = 0;
        if(!itemPerPage) itemPerPage = 10;
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/warehouse/get-list-warehouse?page='+page+'&itemPerPage='+itemPerPage+'&filters='+JSON.stringify(filters), {headers:headers})
            .map(res => res.json())
    }

    getCompany(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/warehouse/get-list-company', {headers:headers})
            .map(res => res.json())
    }

    createWarehouse(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/warehouse/create',body,{headers:headers})
            .map(res=>res.json())
    }

    editWarehouse(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/warehouse/update',body,{headers:headers})
            .map(res=>res.json())
    }

    deleteWarehouse(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/warehouse/delete?id='+id,{headers:headers})
            .map(res=>res.json())
    }

    detailWarehouse(id){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/warehouse/view?id='+id,{headers:headers})
            .map(res=>res.json())
    }
    
}