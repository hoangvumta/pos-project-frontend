export class WareHouses {
    constructor(
        public id: number =null,
        public name: string = null,
        public description: string = null,
        public address: string = null,
        public phone: string = null,
        public province_id: number = 0,
        public district_id: number = 0,
        public ward_id: number = 0,
        public company_id: number = 0,
        public type:number = null

    ) {  }
}