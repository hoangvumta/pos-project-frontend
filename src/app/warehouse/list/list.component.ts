import {Component, Injectable, OnInit} from "@angular/core";
import {ApiService} from "../../helpers/api.service";
import {WareHouseService} from "../warehouse.service";
import {UrlHelper} from "../../helpers/UrlHelper";
declare var $:any;

@Component({
  selector: '[tables-basic]',
  templateUrl: './list.template.html',
  styleUrls: ['./list.style.scss']
})

@Injectable()
export class ListWareHouse implements OnInit {

	constructor(
				private apiService:ApiService,
				private warehouseService:WareHouseService
	)
	{}

	provinces = [];
	companies = [];
	ngOnInit():void {
		this.apiService.getProvince().subscribe(
			(pro)=>{
				pro.unshift({id:0,text:'Tỉnh/Thành Phố'});
				this.provinces = pro;
			}
		)
		this.warehouseService.getCompany().subscribe(
			(com)=>{
				com.unshift({id:0,text:'Doanh nghiệp'});
				this.companies = com;
			}
		)
		this.filterData();
	}

	showLoading = false;
	afterView = false;

	//Config pos-table
	data = [
	];
	settings = {
		columns: [
			{
				label: '#',
				value: (model,index)=>{
					return index+1;
				}
			},
			{
				label: 'ID',
				value: (model)=>{
					return model.id;
				},
				sort: {attribute: 'id'},
				filter: {
					attribute: 'id',
					type: 'input'
				}
			},
			{
				label: 'Tên',
				value: (model)=>{
					return model.name;
				},
				sort: {attribute: 'name'},
				filter: {
					attribute: 'name',
					type: 'input'
				}
			},
			{
				label: 'Doanh nghiệp',
				value: (model)=>{
					return model.company_id;
				},
				filter: {
					attribute: 'company_id',
					type: 'dropdown',
					data: () => {return this.companies}
				}
			},
			{
				label: 'Địa chỉ',
				value: (model)=>{
					let html = '<p>'+model.address+'</p>';
					html += '<p>' +model.ward_id+' -  '+model.district_id+' -  '+model.province_id+' </p>';
					return html;
				},
				sort: {attribute: 'address'},
				filter: {
					attribute: 'address',
					type: 'input'
				},
				format: 'raw'
			},
			{
				label: '',
				value: (model)=>{
					return '';
				},
				filter: {
					attribute: 'province_id',
					type: 'dropdown',
					data: () => {return this.provinces}
				}
			},
			{
				label: 'Thao tác',
				value: (model)=>{
					let html = '<a [routerLink]="[\'/kho-hang/'+model.id+'/cap-nhat\']" class="glyphicon glyphicon-pencil edit"></a>';
					html += '<a class="glyphicon glyphicon-trash delete-store" data-id="'+model.id+'"></a>';
					return html;
				},
				format: 'raw'
			},
		],
		pagination: {
			currentPage: 1,
			maxSize: 10,
			itemsPerPage: 10,
			totalItems: 0,
		}
	}

	pageChanged(event:any):void {
		this.filterData();
	};

	sortChanged(event){
		this.filterData();
	}

	filterChanged(event){
		this.filterData();
	}

	filterData(){
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;

		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();

		this.showLoading = true;
		this.warehouseService.getWarehouse(page,itemPerpage,filters).subscribe(
			res => {
				this.data = res.data;
				this.settings.pagination.totalItems = res.totalItems;
				this.settings.pagination.currentPage = page;
				this.afterView = true;
			},
			error => {
				console.log('Error');
			},
			()=>this.showLoading = false
		)
	}

	//End config pos-table


	ngOnDestroy() {}

	ngAfterViewChecked(){
		if(this.afterView){
			if($('.delete-store').length > 0){
				$('.delete-store').off('click').click((ele)=>{
					let id = $(ele.currentTarget).attr('data-id');
					this.onDelete(id);
				});
				this.afterView = false;
			}else{
			}
		}
	}


	onDelete(id) {
		this.showLoading = true;
		//Phân trang
		let page = UrlHelper.getParams()['page'];
		let itemPerpage = this.settings.pagination.itemsPerPage;

		//End phân trang

		//param sort, filter
		let filters = UrlHelper.getParams();
		this.warehouseService.deleteWarehouse(id).subscribe(
			(res:any) => {
				if(res.success == 1){
					this.filterData();
				}
			},
			error => {
				console.log('Error')
			},
			()=> this.showLoading = false
		)
	}

}