import {Component, Injectable, OnInit, ElementRef, ViewChild} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import { WareHouses} from "../warehouse";
import {WareHouseService} from "../warehouse.service";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {Subscription} from "rxjs/Rx";
import 'select2';
declare var $:any;
@Component({
    selector: 'edit',
    templateUrl: './edit.template.html',
    styleUrls: ['./edit.style.scss']
})
@Injectable()

export class EditWareHouse implements OnInit{
    @ViewChild('edit') selector: ElementRef;
    selected: any;
    idProvince =0;
    idDistrict = 0;
    idWard =0;

    warehouseId = null;
    private subscription: Subscription;
    message =null;
    warning ='';
    danger ='';
    model = new WareHouses();
    submitted = false;

    complexForm : FormGroup;
    public exampleCompany = [];

    loading = false;

    constructor(private router: Router, private route: ActivatedRoute, private warehouseService: WareHouseService){
    }

    ngOnInit(){
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if(params.hasOwnProperty('id')) {
                    this.warehouseId = params.id;
                }else {
                    this.warehouseId = null;
                }
                this.initForm();
            }
        );
        this.warehouseService.getCompany().subscribe(
            data => {
                if(data.length == 1){
                    this.model.company_id = data[0].id;
                }else {
                    data.unshift({id: 0, text: 'Doanh nghiệp'});
                }
                this.exampleCompany = data;
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    validatePhone($att:FormControl) {
    $att = $att.value;
    if($att == null || typeof $att == "undefined") return {phoneValid:false};
    let flag = true;
    let phone = $att.toString();
    //Bắt đầu bằng số 0
    if(parseInt(phone.slice(0,1)) != 0){
        flag = false;
    }
    //Đầu số phải là 09 or 01
    if (phone.slice(0,2) != '01' && phone.slice(0,2) != '09' && phone.slice(0,2) != '08') {
        flag=false;
    }
    //Đầu 09 độ dài = 10
    if(phone.slice(0,2) == '09'){
        if(phone.length != 10) {
            flag = false;
        }
    }

    //Đầu 01 độ dài 11
    if(phone.slice(0,2) == '01'){
        if(phone.length != 11) flag = false;
    }

    //Độ dài 10-11
    if (phone.length < 10 || phone.length > 11) flag = false;

    if(flag == false){

        return {phoneValid:false}
        // this.message = 'Số điện thoại không đúng định dạng';
    }else{
        return null;
    }
}

    initForm() {
        if(this.warehouseId != null){
            this.warehouseService.detailWarehouse(this.warehouseId).subscribe(
                (data:any)=>{
                    this.complexForm = new FormGroup({
                        'name' : new FormControl('',Validators.required ),
                        'phone' : new FormControl('',[
                            Validators.required,
                            this.validatePhone
                        ]),
                        'address' : new FormControl('',Validators.required ),
                        'description' : new FormControl(),
                        'type' : new FormControl('Chọn loại kho',Validators.required ),
                        'id' : new FormControl('',Validators.required )
                    });

                        this.model.id = data.id;
                        this.model.name = data.name;
                        this.model.phone = data.phone;
                        this.model.address = data.address;
                        this.model.description = data.description;
                        this.model.type = data.type;
                        this.model.company_id = data.company_id;

                        this.idProvince = data.province_id;
                        this.idDistrict = data.district_id;
                        this.idWard = data.ward_id;
                },
                (error)=>console.log(error)
            )

        }
    }

    selectProvince($event){
        if($event.value==0) {
            this.model.province_id = null;
        }else {
            this.model.province_id = $event.value;
        }

    }
    selectDistrict($event){
        if($event.value==0) {
            this.model.district_id = null;
        }else {
            this.model.district_id = $event.value;
        }
    }
    selectTown($event){
        if($event.value==0) {
            this.model.ward_id = null;
        }else {
            this.model.ward_id = $event.value;
        }
    }
    onChangeSelectCompany($event){
        if($event.value==0) {
            this.model.company_id = null;
        }else {
            this.model.company_id = $event.value;
        }
    }

    editWarehouse(complexForm){
        this.loading = true;
        if(this.model.province_id == 0 || this.model.district_id == 0 || this.model.ward_id == 0 || this.model.company_id == 0){
            this.message = 'Vui lòng điền đủ thông tin';
            return false ;
        }
        this.warehouseService.editWarehouse(this.model).subscribe(
            res => {
                this.router.navigate(['/kho-hang']);
                this.message = 'Sửa kho thành công';
            },
            err => console.log(err),
            () => this.loading = false
        );
    }

    private afterSelectFilterRender = true;
    ngAfterViewChecked() {
        if($('.filter-select option').length > 0 && this.afterSelectFilterRender){
            $('.filter-select').select2({
                theme: 'bootstrap',
                width: '100%'
            });
            $('.filter-select').change((event)=>{
                this.model.company_id = event.target.value;
                let attr = event.target.attributes.name.value;
                let value = event.target.value;
            })
            this.afterSelectFilterRender =false;
        }
    }
}