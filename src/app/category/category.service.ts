import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import {AppConfig} from '../app.config';
@Injectable()

export class CategoryService{
    constructor(private http: Http, config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    getCat()
    {
        return this.http.get(this.config.baseApiUrl+'/category/get-data')
            .map(res => res.json())
    }
    getList()
    {
        return this.http.get(this.config.baseApiUrl+'/category/get-list')
            .map(res => res.json())
    }
    editCat(id,name,des,parent_id)
    {
        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify({name:name,des:des,id:id,parent_id:parent_id});
        return this.http.post(this.config.baseApiUrl+'/category/update',body,headers)
            .map(res=>res.json())
    }
    newCat(parent_id,name,des)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = JSON.stringify({name:name,des:des,parent_id:parent_id});
        return this.http.post(this.config.baseApiUrl+'/category/create',body,headers)
            .map(res=>res.json())
    }
    deleteCat(id)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this.config.baseApiUrl+'/category/delete?id='+id,headers)
            .map(res=>res.json())
    }

}