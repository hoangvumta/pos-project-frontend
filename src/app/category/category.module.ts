import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {Category} from "./category.component";
import {TreeModule} from 'angular2-tree-component';
import {CategoryService} from './category.service.ts';

export const routes = [
    { path: '', component: Category, pathMatch: 'full' }

];

@NgModule({
    declarations: [
        Category,
    ],
    imports: [
        CommonModule,
        FormsModule,
        TreeModule,
        RouterModule.forChild(routes)
    ],
    providers: [CategoryService]
})
export default class CategoryModule {
    static routes = routes;
}