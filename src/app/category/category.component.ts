import {Component, Input, OnInit} from '@angular/core';
import {TreeComponent, TreeNode, TREE_ACTIONS, KEYS, IActionMapping} from 'angular2-tree-component';
import {ApiService} from "../helpers/api.service";
import {Http, Headers, Response} from '@angular/http';
import {Observable} from "rxjs";
import {NgForm} from "@angular/forms";
import {CategoryService} from "./category.service.ts";

const actionMapping: IActionMapping = {
    mouse: {
        contextMenu: (tree, node, $event) => {
            $event.preventDefault();
            alert(`context menu for ${node.data.name}`);
        },
        dblClick: TREE_ACTIONS.TOGGLE_EXPANDED,
        click: (tree, node, $event) => {
            $event.shiftKey
                ? TREE_ACTIONS.TOGGLE_SELECTED_MULTI(tree, node, $event)
                : TREE_ACTIONS.TOGGLE_SELECTED(tree, node, $event)
        }
    },
    keys: {
        [KEYS.ENTER]: (tree, node, $event) => alert(`This is ${node.data.name}`)
    }
};

@Component({
    selector: 'category',
    styleUrls: ['./category.style.scss'],
    templateUrl: 'category.template.html'
})
export class Category implements OnInit {
    idExpand = 0;
    name = '';
    idSelect = 0;
    nameSelect = '';
    message = '';
    warning = '';
    danger = '';

    nodes: any[] = null;
    arr: any[] = null;
    parents: any = null;
    parents_temp: any
    expanded = [];
    // private tree : TreeComponent;
    constructor(private categoryService: CategoryService, private http: Http) {
        // TODO: Get Category full infomation
        this.categoryService.getCat().subscribe(
            res => {
                this.nodes = res.data

                 console.log(this.nodes);
            },
            error => {
                console.log('Error');
            }
        )
        // TODO: Get id & name Category
        this.categoryService.getList().subscribe(
            res=> {
                this.parents = res.data
                // this.parents_temp = res.data
                 console.log(this.parents);
            },
            error => {
                console.log('Error')
            }
        )
    }

    ngOnInit() {

    }

    // TODO: Constructor model
    model = {
        idCat: 0,
        parentCat: 0,
        nameCat: '',
        description: ''
    }

// TODO: Save Category
    saveCat(form: NgForm, tree) {
        if(this.parents!= null) {
            let i = 0;
            for (i = 0; i < this.parents.length; i++) {
                // Get id expanding
                if (tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).isExpanded == true) {
                    this.expanded.push(tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).data.id)
                }
            }
        }
        // console.log(this.expanded)
        // Get id parent
        this.idExpand = this.model.parentCat
        if (this.model.parentCat == null) {
            this.model.parentCat = 0
        }
        if (this.model.idCat == 0) {
            // TODO: Create new Category

            this.nameSelect = this.model.nameCat
            this.categoryService.newCat(this.model.parentCat, this.model.nameCat, this.model.description).subscribe(
                res => {
                    this.nodes = res.data

                    this.message = "Thêm danh mục dành công"
                },
                error => {
                    console.log('Error');
                }
            )
        }
        else {
            if (this.model.idCat == this.model.parentCat) {
                this.warning = "Không thể chọn danh mục cha là chính nó"
            }
            else {
                // TODO: Update Category
                this.categoryService.editCat(this.model.idCat, this.model.nameCat, this.model.description, this.model.parentCat).subscribe(
                    res => {
                        this.nodes = res.data;
                        // this.idExpand = 0
                        this.idSelect = this.model.idCat
                        this.message = "Sửa danh mục thành công"
                    },
                    error => {
                        console.log('Error');
                    }
                )
            }

        }


        // this.tree.treeModel.update();
    }

    // TODO: Delete Category
    deleteCat(id: number, tree) {
        if (id == 0) {
            this.warning = 'Chọn danh mục để xóa'
        }
        else {

            // console.log(this.expanded)
            this.categoryService.deleteCat(id).subscribe(
                res => {
                    this.nodes = res.data;
                    this.danger = "Xóa danh mục thành công"
                },
                error => {
                    console.log('Error');
                }
            )
            if(this.parents != null) {
                let i = 0;
                for (i = 0; i < this.parents.length; i++) {
                    // Get id is expanding

                    if (tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).isExpanded == true && tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).data.id != id) {
                        // console.log(tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).data.id)
                        this.expanded.push(tree.treeModel.getNodeBy((node)=> node.data.id == this.parents[i].id).data.id)
                    }
                }
            }
        }
    }

    expand(tree) {
        this.categoryService.getList().subscribe(
            res=> {
                this.parents = res.data;

                console.log(this.parents);
            },
            error => {
                console.log('Error')
            }
        )
        // Expand root
        tree.treeModel.getNodeBy((node) => node.data.id == 0)
            .toggleExpanded();

        if (this.idExpand != 0) {
            tree.treeModel.getNodeBy((node) => node.data.id == this.idExpand)
                .toggleExpanded();
        }

        console.log(this.idExpand)
        if(this.expanded != null) {
            let i = 0;
            for (i = 0; i < this.expanded.length; i++) {
                // console.log(this.expanded[i])
                if (this.idExpand != this.expanded[i] && this.idExpand != 0) {
                    tree.treeModel.getNodeBy((node) => node.data.id == this.idExpand)
                        .toggleExpanded();
                }
                if (this.idExpand != this.expanded[i]) {
                    tree.treeModel.getNodeBy((node) => node.data.id == this.expanded[i])
                        .toggleExpanded();
                }

            }
        }
        this.idExpand = 0
        this.expanded = []
        if (this.nameSelect != '') {
            tree.treeModel.getNodeBy((node)=>node.data.name == this.nameSelect)
                .focus();
        }
        this.nameSelect = '';
// console.log(this.idSelect)
        if (this.idSelect != 0) {
            tree.treeModel.getNodeBy((node) => node.data.id == this.idSelect)
                .focus();

        }
        this.idSelect = 0;
    }

    asyncChildren = [
        {
            name: 'child2.1',
            subTitle: 'new and improved'
        }, {
            name: 'child2.2',
            subTitle: 'new and improved2'
        }
    ];


    addNode(tree) {
        this.nodes["children"][0].children.push({

            name: 'a new child'
        });
        tree.treeModel.update();
    }

    childrenCount(node: TreeNode): string {
        return node && node.children ? `(${node.children.length})` : '';
    }

    filterNodes(text, tree) {
        tree.treeModel.filterNodes(text, true);
    }

    activateSubSub(tree) {
        tree.treeModel.getNodeBy((node) => node.data.name === 'subsub')
        // tree.treeModel.getNodeById(1001)
            .setActiveAndVisible();
    }

    toggle(tree) {
        tree.treeModel.getNodelById(1).expand()
    }

    customTemplateStringOptions = {
        displayField: ['subTitle', 'parent'],
        // isExpandedField: 'expanded',
        idField: 'uuid',
        // getChildren: this.getChildren.bind(this),
        actionMapping,
        allowDrag: false
    }
    onEvent = console.log.bind(console);

    addTree(parent_id: number) {
        this.model.idCat = 0;
        this.model.parentCat = parent_id;
        this.model.nameCat = '';
        this.model.description = '';
    }

    go(id: number, name: string, des: string, parent: number) {
        this.message = ''
        this.danger = ''
        this.warning = ''
        this.model.idCat = id;
        this.model.nameCat = name;
        this.model.description = des;
        this.model.parentCat = parent
    }

}