import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {UserService} from "./user.service";

import { User } from './user.component';

export const routes = [
  { path: 'cap-nhat', component: User}
];

@NgModule({
  declarations: [
    User
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [UserService]
})
export default class UserModule {
  static routes = routes;
}
