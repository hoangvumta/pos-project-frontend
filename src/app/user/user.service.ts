import { Injectable } from "@angular/core";
import {Http, Headers} from "@angular/http";
import { LocalStorage } from '../helpers/localstorage.service';
import {DataService} from "../helpers/data.service";
import {AppConfig} from '../app.config';

@Injectable()

export class UserService{
    constructor(private http: Http, private localStorageService: LocalStorage, private dataService:DataService , config: AppConfig){
        this.config = config.getConfig();
    }

    config: any;

    editUser(model){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        let body = JSON.stringify(model);
        return this.http.post(this.config.baseApiUrl+'/user/change-password',body,{headers:headers})
            .map(res=>res.json())
    }

    detailUser(){
        let token = this.localStorageService.get('token');
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        headers.append('Content-Type','application/x-www-form-urlencoded');
        return this.http.get(this.config.baseApiUrl+'/user/who-am-i',{headers:headers})
            .map(res=>res.json())
    }



}