import {Component, Injectable, OnInit, ViewEncapsulation} from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Users} from "./user.ts";
import {FormGroup, Validators, FormControl } from '@angular/forms';
import {UserService} from "./user.service";
import {Subscription} from "rxjs/Rx";
import { LocalStorage } from '../helpers/localstorage.service';
import {DataService} from "../helpers/data.service";


@Component({
    selector: 'user',
    styleUrls: [ './user.style.scss' ],
    templateUrl: './user.template.html',
    encapsulation: ViewEncapsulation.None,
    host: {
        class: 'user-page app'
    }
})
@Injectable()
export class User implements OnInit{
    constructor( private userService: UserService, private route: ActivatedRoute, private router: Router, private localStorageService: LocalStorage, private dataService:DataService ) {}

    complexForm : FormGroup;
    model = new Users();
    userId = null;
    private subscription: Subscription;
    loading = false;
    message = '';
    notification = '';

    ngOnInit() {
        this.initForm();
    }
    ngOnDestroy() {
        //this.subscription.unsubscribe();
    }

    initForm() {
        this.userService.detailUser().subscribe(
            (data:any)=>{
                this.complexForm = new FormGroup({
                    'name' : new FormControl('',Validators.required ),
                    'password' : new FormControl('',Validators.required ),
                    'id' : new FormControl('',Validators.required )
                });
                this.model.id = data.id;
                this.model.name = data.name;
                this.model.password = data.password;
            },
            (error)=>console.log(error)
        )
    }

    editUserForm(complexForm){
        this.loading = true;
        this.userService.editUser(this.model).subscribe(
            res => {
                this.localStorageService.set('token', res.token);
                this.dataService.setAuthStatus(true);
                this.notification = 'Đổi mật khẩu thành công!';
            },
            err => console.log(err),
            () => this.loading = false
        );
    }
}