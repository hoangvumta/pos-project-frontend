import { Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { TokenGuard, AuthGuard } from './helpers/guards';

export const ROUTES: Routes = [
	{path: '', loadChildren: () => System.import('./layout/layout.module')},
	{path: 'register', loadChildren: () => System.import('./register/register.module')},
	{path: 'login', loadChildren: () => System.import('./login/login.module'), canActivate: [TokenGuard]},
	{path: 'error', component: ErrorComponent}, {
    path: '**',    component: ErrorComponent
  }
];
