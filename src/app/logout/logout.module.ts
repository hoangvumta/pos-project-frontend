import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Logout } from './logout.component';

export const routes = [
    { path: '', component: Logout, pathMatch: 'full' }
];

@NgModule({
    declarations: [
        Logout
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
    ]
})
export default class LogoutModule {
    static routes = routes;
}
