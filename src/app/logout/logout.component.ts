import { Component, ViewEncapsulation } from '@angular/core';
import { ApiService } from '../helpers/services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'logout',
    template: `<logout></logout>`,
    encapsulation: ViewEncapsulation.None,
})
export class Logout {
    constructor( private apiService: ApiService, private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {
        this.apiService.logout();
        this.router.navigateByUrl(`/login`);
    }

}